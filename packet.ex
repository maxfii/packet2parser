defmodule LM2.Model.Packet do
    require Bitwise

    @blacklist_map [
        :RsCharMoveStart, :RsCharMove, :RsCharMoveOffset, :RsCharMoveStop,
        :HeartBeat,
        :NtCharAppear, :NtCharDisappear,
        :NtSkillCastBegin, :NtSkillEffect, :NtSkillCastEnd,
        :NtSkillFireProjectile,
        :NtHpUpdate, :NtMpUpdate, :NtExpUpdate, :NtKarmaUpdate,
        :NtUpdateVitality,
        :NtAchievementUpdateCount,
        :NtItemInfoUpdate,

        :NtQuestUpdateProgress, :NtBasicAtkToggleSwitch,

        :NtCharLookAt, :NtCharDir, :NtAbnormalEffectFx, :NtCharStateUpdate,
        :NtAbnormalAdd, :NtAbnormalRemove,
        :NtMoveSpeedUpdate,
        :NtMonsterOccupied, :NtMonsterFirstAggro,
        :NtMonsterAppear, :NtNpcAppear, :NtFOAppear,
        :NtCharDisappear, :NtFODisappear, :NtCharDie, :NtFODie, :NtNpcDisappear,
        :NtServerMsg,

        :NtDropItem, :NtDropItemRemove,
        :RsItemUse,
        :RsSkillCast, :NtBasicAtkReserveComplete,
        :NtItemEquipChange
    ] |> Enum.into(%{}, fn(op)-> {op, 1} end)

    def encode_size(x) when is_binary(x) do
        plen = 4 + byte_size(x)
        encode_varint(plen)
    end

    def encode_size(x) when is_integer(x) do
        plen = 4 + x
        encode_varint(plen)
    end

    def encode_varint(n) do encode_varint(n, "") end
    def encode_varint(plen, acc) when is_integer(plen) do
        next_char = Bitwise.band plen, 0x7f
        continue = Bitwise.>>> plen, 7
        next_char = if continue > 0 do Bitwise.bor next_char, 0x80 else next_char end
        acc = acc <> <<next_char>>

        if (continue > 0) do
            encode_varint(continue, acc)
        else
            acc
        end
    end

    def varint_read(<<h, r::binary>>), do: varint_read(<<h, r::binary>>, {0,0,0})
    def varint_read("", {plen, slen, bits}) do {999999999, 4} end
    def varint_read(<<h, r::binary>>, {plen, slen, bits}) do
        plen = Bitwise.bor plen, (Bitwise.<<< Bitwise.band(h, 0x7f), bits)
        if (Bitwise.band h, 0x80) == 0x80 do
            varint_read(r, {plen, slen + 1, bits + 7})
        else
            {plen, slen + 1}
        end
    end

    def encode(pkt) do
        encode_size(pkt) <> pkt
    end

    def decode_buffer("", _, _, acc) do
        {"", acc}
    end
    def decode_buffer(buf, is_lounge\\false, rc4stream\\nil, acc\\[]) do
        #TODO: Fragmentation?
        {plen, slen} = varint_read(buf, {0,0,0})
        payload_len = plen - 4
        if payload_len > byte_size(buf) - slen do
            {buf, acc}
        else
            <<x::binary-size(slen), payload::binary-size(payload_len), rest::binary>> = buf
            <<opcode::16-little, payload::binary>> = if rc4stream != nil and rc4stream != :server do
                {_, decoded} = :crypto.stream_encrypt rc4stream, payload
                decoded
            else
                payload
            end
            if opcode != 4608 do #NtMapFrame
                op_atom = if is_lounge, do: Opcodes.lounge_opcode(opcode), else: Opcodes.opcode(opcode)
                if !@blacklist_map[op_atom] do
                    FL.inspect {:decoded_packet, op_atom, payload}, limit: :infinity, printable_limit: :infinity
                end
                pkt = Packet.parse(op_atom, payload)
                pkt = Map.put(pkt, :_op, op_atom)
                decode_buffer(rest, is_lounge, rc4stream, acc ++ [pkt])
            else
                decode_buffer(rest, is_lounge, rc4stream, acc)
            end
        end
    end
end

defmodule Packet do
    import ShorterMaps
    require Bitwise

    def parse_item_struct(rest) do
        # 196, 96, 10, 0, 0, 104, 0, 0, 33, 130, 103, 30, 172, 2, 1, 34, 92, 143, 194, 245, 40, 92, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        # 11, 179, 13, 0, 0, 159, 0, 0, 67, 35, 111, 30, 10,      1, 184, 245, 40, 92, 143, 194, 245,    1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        # 204, 96, 12, 0, 0, 203, 0, 0, 65, 35, 111, 30, 10,      1, 134, 194, 245, 40, 92, 143, 194,    1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        # 142, 61, 11, 0, 0, 122, 0, 0, 1, 233, 164, 53, 232, 7,  1, 45, 51, 51, 51, 51, 51, 51,     1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,

        {container_items_count, rest} = Proto.varint_read rest
        {rest, items} = if container_items_count == 0 do
            {rest, []}
        else
            Enum.reduce 1..container_items_count, {rest, []}, fn(_, {rest, acc})->
                <<item_key :: 64-little,
                  item_id :: 32-little,
                  rest :: binary >> = rest
                {item_count, rest} = Proto.varint_read rest
              <<
              container_type,
              slot_order :: 64-little,
              is_locked,
              period_expire_time_local :: 64-little,
              increase_reason :: 32-little,
              decrease_reason,
                rest :: binary >> = rest

                item = {item_key, item_id, item_count,
                container_type, slot_order, is_locked,
                period_expire_time_local, increase_reason, decrease_reason}
                {rest, acc ++ [item]}
            end
        end

        {equip_items_count, rest} = Proto.varint_read rest
        # 247, 34, 14, 0, 0, 119, 0, 0, 102, 124, 149, 6, 1, 1, 12, 174, 71, 225, 122, 20, 174, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        # 59, 233, 13, 0, 0, 44, 0, 0, 129, 139, 114, 24, 184, 23, 1, 23, 133, 235, 81, 184, 30, 133, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        {rest, equip_items} = if equip_items_count == 0 do
            {rest, []}
        else
            Enum.reduce 1..equip_items_count, {rest, []}, fn(_, {rest, acc})->
              <<item_key :: 64-little,
                item_id :: 32-little,
                rest :: binary >> = rest
              {item_count, rest} = Proto.varint_read rest
              <<
                container_type,
                slot_order :: 64-little,
                is_locked,
                period_expire_time_local :: 64-little,
                increase_reason :: 32-little,
                decrease_reason,

                enchant_level,
                is_break,
                equip_slot,
                smelt_stat1,
                smelt_value1 :: 32-little,
                smelt_stat2,
                smelt_value2 :: 32-little,
                smelt_stat3,
                smelt_value3 :: 32-little,
                rest :: binary >> = rest

                item = {
                    item_key, item_id, item_count,
                    container_type, slot_order, is_locked,
                    period_expire_time_local, increase_reason, decrease_reason,
                    enchant_level, is_break, equip_slot, smelt_stat1, smelt_value1,
                    smelt_stat2, smelt_value2, smelt_stat3, smelt_value3,
                }

                {rest, acc ++ [item]}
            end

        end

        equip_mapped = Enum.map(equip_items, fn(ei)->
            item_id = elem(ei,1)
            type_map = Map.merge(Type.item(item_id), ItemTables.item(item_id))
            type_map = Map.delete(type_map, :extra)
            Map.merge(%{id: elem(ei,0), type_id: item_id, amount: elem(ei,2),
                enchant_lvl: elem(ei,9), equipped_slot: Type.slot(elem(ei,11)), equippable: true},
                type_map)
        end)
        items_mapped = Enum.map(items, fn(ei)->
            item_id = elem(ei,1)
            type_map = Map.merge(Type.item(item_id), ItemTables.item(item_id))
            type_map = Map.delete(type_map, :extra)
            Map.merge(%{id: elem(ei,0),
                type_id: item_id, amount: elem(ei,2)}, type_map)
        end)
        items_mapped = items_mapped ++ equip_mapped
        {items, equip_items, items_mapped, rest}
    end

    def parse(n, buf) when is_integer(n) do
        case Opcodes.opcode(n) do
            sopcode when is_atom sopcode ->
                parse(sopcode, buf)
            _ ->
                %{error: true, unkown: true, bin: buf}
        end
    end

    def parse(:"RqNPLogin", buf) do
        <<error::32-little, s1_size, s1::binary-size(s1_size), error2, sid, 0,
            s2_size, s2::binary-size(s2_size), _::binary
        >> = buf
        %{s1: s1, s2: s2, error: error, error2: error2, server_id: sid}
    end

<<0, 0, 0, 0,
36, 65, 66, 53, 52, 54, 55, 55, 69, 45, 70, 50, 68, 57, 45, 52,
65, 53, 48, 45, 56, 55, 53, 66, 45, 70, 51, 52, 54, 65, 65, 49, 67, 57, 52,70, 67,
     2, 10, 0,
     17, 51, 56, 56, 53, 54, 53, 55, 58, 49, 56, 56, 57, 50,
     54, 54, 52, 51, 0, 0>>

    def parse(:"RsMoveToServer", buf) do
        <<error::32-little, serverid::16-little, asize,
            gamehost::binary-size(asize), gameport::16-little
        >> = buf
        %{serverid: serverid, gamehost: gamehost, gameport: gameport, error: error}
    end

    def parse(:"RsCharCreate", buf) do
        case buf do
            <<0, 0, 0, 0, char_id::little-32, ns, _::binary>> ->
                %{char_id: char_id, ns: ns, error: :ok}
            <<52, 32, _::binary>> ->
                %{error: :name_exists}
            #name too long?   "괴물쥐라기초가스"
            #<<50, 32, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  # 0, 0, 0, 0, 0, 0, 0, 216, 154, 182, 55, 71, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  # 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 152, 142, 182, 55, 71, 2, 0, 0, 0, 56,
  # 95, 226, 133, 70, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0>>
        end
    end

    def parse(:NtMyCharAppear, data) do
        <<243, 72,

        6, #flags

        1, #has_nickname

        10, 109, 97, 114, 99, 111, 97, 107, 105, 111, 112,

        69, 47, 0,
          0, 1, 1, 1, 1, 0, 0, 0, 178, 216, 1, 0, 178, 216, 1, 0,
          6, 1, 0, 0, 0, 0, 2, 0, 0, 0, 0, 3, 0, 0, 0, 0, 4, 0, 0, 0, 0, 5, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0,

          192, 165, 107, 200, #x
          0, 74, 65, 72, #y
          2, 161, 169, 69, #z

          1, 1, 186, 2, 186, 2,
          231, 3, 231, 3, 0, 3, 1, 1, 0, 0, 0, 193, 168, 50, 0, 255, 255, 255, 255, 15,
          128, 117, 213, 42, 187, 3, 0, 0, 115, 36, 0, 0, 1, 44, 111, 146, 6, 0, 1, 0,
          1, 92, 0, 0, 0, 144, 3, 0, 0, 201, 44, 0, 0, 0, 0, 75, 0>>


# 7ff7879dcf92 48 8d 4b 18     LEA        RCX,[RBX + 0x18]
# 7ff7879dcf96 e8 b5 2c        CALL       read_object
# fa ff
# 7ff7879dcf9b 48 8d 4b 78     LEA        RCX,[RBX + 0x78]
# 7ff7879dcf9f e8 ac 2c        CALL       read_object
# fa ff
# 7ff7879dcfa4 40 f6 c7 01     TEST       DIL,0x1
# 7ff7879dcfa8 74 0c           JZ         LAB_7ff7879dcfb6
# 7ff7879dcfaa 48 8d 8b        LEA        RCX,[RBX + 0xc0]
# c0 00 00 00
# 7ff7879dcfb1 e8 9a 2c        CALL       read_object
# fa ff


        {id, rest} = Proto.varint_read data
        {flags_0, rest} = Proto.read_byte rest

        #first object
        {has_nickname, rest} = Proto.read_byte rest
        {nickname, rest} = if has_nickname != 0 do
            Proto.read_byteArray rest
        else
            {nil, rest}
        end
        <<
         pc_id :: 32-little,
         flags_2,
         rest :: binary
         >> = rest
         {char_level, rest} = Proto.varint_read rest
         {level_max, rest} = Proto.varint_read rest
         <<
           race_pc_id :: 32-little,
           base_classcard :: 32-little,
           last_using_classcard :: 32-little,
           rest :: binary
          >> = rest
        {bonus_stat, rest} = if (Bitwise.band(flags_2,1) == 1) do
            {bonus_count, rest} = Proto.varint_read rest
            if bonus_count > 0 do
                Enum.reduce 1..bonus_count, {[], rest}, fn(_, {acc, rest})->
                    <<bonus_id, bonus::32-little, rest::binary>> = rest
                    {[{bonus_id, bonus} | acc], rest}
                end
            else
                {[], rest}
            end
        else
            {[], rest}
        end
        {exp, rest} = Proto.varint_read rest
        {last_using_agathion_id, rest} = if (Bitwise.band(flags_2,2) != 0) do
            <<
            last_using_agathion_id :: 32-little,
            rest :: binary >> = rest
            {last_using_agathion_id, rest}
        else
            {0, rest}
        end

        #second vtabled function
        <<flags2,
        x::float-32-little, y::float-32-little, z::float-32-little,
        dir,
        live,
        rest::binary>> = rest

        {hp_cur, rest} = Proto.varint_read rest
        {hp_max, rest} = Proto.varint_read rest
        {mp_cur, rest} = Proto.varint_read rest
        {mp_max, rest} = Proto.varint_read rest

        {karma, rest} = if (Bitwise.band(flags2,1) != 0) do
            <<k::16-little, rest::binary>> = rest
            {k, rest}
        else
            {0, rest}
        end
        {char_state, rest} = if (Bitwise.band(flags2,2) != 0) do
            <<k, rest::binary>> = rest
            {k, rest}
        else
            {0, rest}
        end
        {toggle_flags, rest} = if (Bitwise.band(flags2,4) != 0) do
            <<k::32-little, rest::binary>> = rest
            {k, rest}
        else
            {0, rest}
        end
        {stat_move_speed, rest} = Proto.varint_read rest
        {phase, rest} = if (Bitwise.band(flags2,8) != 0) do
            <<k, rest::binary>> = rest
            {k, rest}
        else
            {0, rest}
        end
        {seq_frame, rest} = if (Bitwise.band(flags2,0x10) != 0) do
            <<k::16-little, rest::binary>> = rest
            {k, rest}
        else
            {0, rest}
        end
        {seq_idx, rest} = if (Bitwise.band(flags2,0x20) != 0) do
            <<k, rest::binary>> = rest
            {k, rest}
        else
            {0, rest}
        end
        <<relationship_case, rest::binary>> = rest

        {guild_desc, rest} = if (Bitwise.band(flags_0,1) != 0) do
            throw :read_guild
        else
            {nil, rest}
        end

        {abnormals, rest} = if (Bitwise.band(flags_0,2) != 0) do
            {abnormals, rest} = readlist rest, fn(x)->
                parse_abnormal x
            end
            {abnormals, rest}
        else
            {nil, rest}
        end

        {equipment, rest} = if (Bitwise.band(flags_0,4) != 0) do
            {equipment, rest} = readlist rest, fn(x)->
                <<item_id::32-little,
                enchant_level,
                equip_slot,
                period_expire_time_local::64-little,
                is_period_expired,
                rest :: binary
                >> = x

                {{item_id, enchant_level, equip_slot}, rest}
            end
            {equipment, rest}
        else
            {nil, rest}
        end

        <<reason,
        equip_setting_no,
        map_id::32-little,
        zone_id::32-little,
        rest::binary>> = rest

        {agathion_id, rest} = if (Bitwise.band(flags_0,8) != 0) do
            <<k::32-little, rest::binary>> = rest
            {k, rest}
        else
            {0, rest}
        end
        <<dbid::64-little, _::binary>> = rest

        Process.put(:char_id, id)
        %{char_id: id, bonus_stat: bonus_stat, exp: exp, level: char_level,
            race: race_pc_id, card_id: last_using_classcard,
            pet_id: last_using_agathion_id,
            live: live, map_id: map_id, zone_id: zone_id,
            hp_max: hp_max, hp_cur: hp_cur, mp_max: mp_max, mp_cur: mp_cur,
            pos: {trunc(x), trunc(y), trunc(z)},
            char_state: char_state,
            relationship_case: relationship_case,
            abnormals: abnormals,
            equipment: equipment,
            map_id: map_id,
            zone_id: zone_id,
            dbid: dbid} #
    end

    def parse_abnormal(data) do
        # read_uint32((int *)(in_RCX + 0x10)); FUN_7ff7879b4170(param_2,"_uid",(longlong *)(param_1 + 0x10));
        # read_uint32((int *)(in_RCX + 0x14)); FUN_7ff7879b4170(param_2,"_abnormal_id",(longlong *)(param_1 + 0x14));
        # read_varqword((void *)(in_RCX + 0x18)); FUN_7ff78797f5e0(param_2,"_duration_ms",param_1 + 0x18);
        # read_qword(in_RCX + 0x20); FUN_7ff78797f5e0(param_2,"_expire_time",param_1 + 0x20);
        # read_uint32((int *)(in_RCX + 0x28)); FUN_7ff7879b4640(param_2,"_caster_key",param_1 + 0x28);

        <<
        uid::32-little,
        abnormal_id::32-little,
        rest::binary>> = data
        {duration_ms, rest} = Proto.varint_read rest
        <<
        expire_time::64-little,
        caster_key::32-little,
        rest::binary>> = rest

        type = Type.abnormal(abnormal_id)
        {%{uid: uid,
            abnormal_id: abnormal_id, duration_ms: duration_ms,
            expire_time: expire_time, caster_key: caster_key, type: type}, rest}
    end

    def parse(:RsRetrievePushRewardInfo, rest) do
        <<ukn::32-little, ukn1::64-little, rest::binary>> = rest
        {count, rest} = Proto.varint_read rest

        itr = :lists.seq(0, count-1)
        {rest, push_rewards} = Enum.reduce itr, {rest, []}, fn(_, {rest, acc})->
            <<ukn2, ukn3::32-little, ukn4, rest::binary>> = rest

            {count, rest} = Proto.varint_read rest
            <<text1::binary-size(count), rest::binary>> = rest
            {count, rest} = Proto.varint_read rest
            <<text2::binary-size(count), rest::binary>> = rest
            {count, rest} = Proto.varint_read rest
            <<text3::binary-size(count), rest::binary>> = rest
            <<time1::64-little, time2::64-little, item_id::32-little, amount::64-little,
                rest::binary>> = rest

            <<id::48-little>> = <<ukn2, ukn3::32-little, ukn4>>
            reward = %{
                id: id,
                ukn2: ukn2, ukn3: ukn3, ukn4: ukn4,
                text1: text1, text2: text2, text3: text3,
                time1: time1, time2: time2, item_id: item_id, amount: amount}
            {rest, acc ++ [reward]}
        end
        %{push_rewards: push_rewards}
    end
    def parse(:RsTakePushReward, rest) do
        <<error::32-little, id::48-little, _::binary>> = rest
        %{error: error, id: id}
    end


    #def parse(:RsAgathionLock, rest) do
        #unit32, byte, unit32 id lock result
    #end

    def parse(:RsAgathions, rest) do
# read_uint32((void *)(param_1 + 0x10));
# read_uint32((void *)(param_1 + 0x14));
# *(undefined *)(param_1 + 0x18) = **(undefined **)(lVar1 + 0x20);
# *(undefined *)(param_1 + 0x19) = **(undefined **)(lVar1 + 0x20);
        {agathions, ""} = readlist rest, fn(rest)->
            <<
            id::32-little,
            count::32-little,
            locked,
            auto_summon,
            rest::binary
            >>  = rest
            {%{id: id, count: count, locked: locked, auto_summon: auto_summon}, rest}
        end
        %{pets: agathions}
    end
    def parse(:NtAgathionChanged, data) do
        {char_id, <<id::32-little>>} = Proto.varint_read data
        %{char_id: char_id, id: id}
    end
    def parse(:RsAgathionCombine, rest) do
        #:RsAgathionCombine, <<4, 100, 0, 0, 0, 5, 0, 0, 0, 102, 0, 0, 0, 2, 0, 0, 0, 103, 0, 0, 0, 3, 0, 0, 0, 106, 0, 0, 0, 3, 0, 0, 0, 1, 100, 0, 0, 0, 1, 0, 0, 0, 6, 0, 0, 0, 0>>
        {agathions, _} = readlist rest, fn(rest)->
            <<
            id::32-little,
            count::32-little,
            rest::binary
            >>  = rest
            {%{id: id, count: count}, rest}
        end
        %{changed_pets: agathions}
    end


    def parse(:NtClassCardGacha, rest) do
        #<<1, 92, 63, 5, 0, 1, 0, 0, 0, 1, 92, 63, 5, 0, 1, 0, 0, 0, 0, 0, 0, 0, 240, 135, 64, 32>>
        {cards, rest} = readlist rest, fn(rest)->
            <<
            id::32-little,
            count::32-little,
            rest::binary
            >>  = rest
            {%{id: id, count: count}, rest}
        end
        %{cards: cards}
    end
    def parse(:RsClassCards, rest) do
        {base_cards, rest} = readlist rest, fn(rest)->
            <<
            id::32-little,
            rest::binary
            >>  = rest
            {%{id: id}, rest}
        end
        {cards, ""} = readlist rest, fn(rest)->
            <<
            id::32-little,
            count::32-little,
            ukn,
            rest::binary
            >>  = rest
            {%{id: id, count: count, ukn: ukn}, rest}
        end
        %{cards: cards, base_cards: base_cards}
    end
    def parse(:RsClassCardCombine, rest) do
        #:RsClassCardCombine, <<2, 186, 42, 2, 0, 1, 0, 0, 0, 82, 220, 3, 0, 1, 0, 0, 0, 1, 82, 220, 3, 0, 1, 0, 0, 0, 6, 0, 0, 0, 0>>
        {cards, _} = readlist rest, fn(rest)->
            <<
            id::32-little,
            count::32-little,
            rest::binary
            >>  = rest
            {%{id: id, count: count}, rest}
        end
        %{changed_cards: cards}
    end
    def parse(:RsClassCardTransform, rest) do
        <<id::32-little, error::32-little>> = rest
        %{id: id, error: error}
    end

    def parse(:NtNpcAppear, <<count, data::binary>>) do
         # <<count, data::binary>> = <<5,
         #   173, 216, 0, 0, 0, 0, 0, 0, 100, 137, 30, 0, 0, 60, 156, 200, 128, 8, 72, 72, 2, 24, 171, 69, 0, 224, 68, 66,
         #   174, 216, 0, 0, 0, 0, 0, 0, 103, 137, 30, 0, 224, 48, 156, 200, 64, 40, 72, 72, 4, 24, 171, 69, 0, 172, 128, 67,
         #   182, 216, 0, 0, 0, 0, 0, 0, 102, 137, 30, 0, 192, 242, 155, 200, 0, 107, 71, 72, 132, 242, 170, 69, 0, 176, 154, 66,
         #   183, 216, 0, 0, 0, 0, 0, 0, 101, 137, 30, 0, 160, 173, 156, 200, 0, 35, 71, 72, 2, 49, 171, 69, 0, 48, 177, 66,
         #   220, 216, 0, 0, 0, 0, 0, 0, 75, 137, 30, 0, 96, 224, 156, 200, 0, 3, 72, 72, 66, 236, 170, 69, 0, 176, 154, 66>>

        {rest, newnpcs} = Enum.reduce 1..count, {data, []}, fn(_, {data, acc})->
            <<id::64-little, type::32-little,
                x::float-32-little,y::float-32-little, z::float-32-little,
                angle::float-32-little, rest::binary>> = data
            {rest, acc ++ [%{id: id, type: type, pos: {trunc(x), trunc(y), trunc(z)}, angle: angle}]}
        end

        %{count: count, newnpcs: newnpcs}
    end
    def parse(:NtNpcDisappear, <<count, data::binary>>) do
        #{:server, 1576295746562346, :NtNpcDisappear, <<1, 73, 14, 2, 0, 0, 0, 0, 0>>}
        {rest, newnpcs} = Enum.reduce 1..count, {data, []}, fn(_, {data, acc})->
            <<id::64-little, rest::binary>> = data
            {rest, [id | acc]}
        end

        %{count: count, ids: newnpcs}
    end

    def parse(:NtAbnormalAdd, data) do
        {id, <<count, rest::binary>>} = Proto.varint_read data
        {rest, abnormals} = Enum.reduce 1..count, {rest, []}, fn(_, {rest, acc})->
            <<uid::32-little, abnormal_id::32-little, rest::binary>> = rest
            {duration_ms, rest} = Proto.varint_read rest
            <<expire_time::64-little, caster_key::32-little>> = rest
            type = Type.abnormal(abnormal_id)
            abnormal = %{type: type, uid: uid, abnormal_id: abnormal_id,
                duration_ms: duration_ms, expire_time: expire_time, caster_key: caster_key}
            {rest, acc ++ [abnormal]}
        end

        %{char_id: id, abnormals: abnormals}
    end
    def parse(:NtAbnormalChange, data) do
        {id, rest} = Proto.varint_read data

        <<uid::32-little, abnormal_id::32-little, rest::binary>> = rest
        {duration_ms, rest} = Proto.varint_read rest

        <<expire_time::64-little, caster_key::32-little>> = rest

        type = Type.abnormal(abnormal_id)
        %{type: type, char_id: id, uid: uid, abnormal_id: abnormal_id, duration_ms: duration_ms, expire_time: expire_time, caster_key: caster_key}
    end
    def parse(:NtAbnormalRemove, data) do
        {id, rest} = Proto.varint_read data
        <<uid::32-little>> = rest
        %{char_id: id, uid: uid}
    end

    def parse(:NtItemEquipChange, data) do
        {id, rest} = Proto.varint_read data

        <<slot,

        before_item_id::32-little,
        before_enchant_lvl,
        before_equip_slot,
        before_period_expire_time_local::64-little,
        before_is_period_expired,

        after_item_id::32-little,
        after_enchant_lvl,
        after_equip_slot,
        after_period_expire_time_local::64-little,
        after_is_period_expired,

        >> = rest

        %{id: id, slot: slot, before_item_id: before_item_id,
            before_enchant_lvl: before_enchant_lvl,
            before_equip_slot: Type.slot(before_equip_slot),
            before_period_expire_time_local: before_period_expire_time_local,
            before_is_period_expired: before_is_period_expired,
            after_item_id: after_item_id,
            after_enchant_lvl: after_enchant_lvl,
            after_equip_slot: Type.slot(after_equip_slot),
            after_period_expire_time_local: after_period_expire_time_local,
            after_is_period_expired: after_is_period_expired
        }
    end

    def parse(:RqBuyBMShopDisplayGoods, data) do
        <<
        shop_id::32-little,
        item_id::32-little,
        amount::32-little>> = data

        %{shop_id: shop_id, item_id: item_id, amount: amount}
    end

    def parse(:RqItemEnchant, data) do
        <<
        scroll_uid::64-little,
        equip_uid::64-little,
        >> = data

        %{scroll_uid: scroll_uid, equip_uid: equip_uid}
    end

    def parse(:RsItemEnchant, data) do
        <<
        result::32-little,
        scroll_uid::64-little,
        equip_uid::64-little,
        >> = data

        %{result: result, scroll_uid: scroll_uid, equip_uid: equip_uid}
    end

    def parse(:RqItemSmelt, data) do
        <<
        scroll_uid::64-little,
        equip_uid::64-little,
        is_curse
        >> = data

        %{scroll_uid: scroll_uid, equip_uid: equip_uid, is_curse: is_curse}
    end

    def parse(:RsItemSmelt, data) do
        <<
        result::32-little,
        scroll_uid::64-little,
        equip_uid::64-little,
        is_curse
        >> = data

        %{result: result, scroll_uid: scroll_uid, equip_uid: equip_uid, is_curse: is_curse}
    end

    def parse(:RqTradeSell, data) do
        # read_qword(param_1 + 0x10);FUN_7ff7879bc600(param_2,"_sale_item_key",param_1 + 0x10);
        # read_qword(param_1 + 0x18);FUN_7ff78797f5e0(param_2,"_sale_item_count",param_1 + 0x18);
        # read_qword(param_1 + 0x20);FUN_7ff78797f5e0(param_2,"_sale_price_item_count",param_1 + 0x20);
        # read_qword(param_1 + 0x28);FUN_7ff78797f5e0(param_2,"_reg_fee_rate",param_1 + 0x28);
        # read_qword(param_1 + 0x30);FUN_7ff78797f5e0(param_2,"_reg_fee_min",param_1 + 0x30);
        <<
            sale_item_key::64-little,
            sale_item_count::64-little,
            sale_price_item_count::64-little,
            reg_fee_rate::64-little,
            reg_fee_min::64-little
        >> = data
        %{
            sale_item_key: sale_item_key,
            sale_item_count: sale_item_count,
            sale_price_item_count: sale_price_item_count,
            reg_fee_rate: reg_fee_rate,
            reg_fee_min: reg_fee_min
        }
    end

    def parse(:RsTradeSell, data) do
        # read_uint32((int *)(param_1 + 0x10));FUN_7ff7879bb770(param_2,"_result",param_1 + 0x10);
        # read_bytearray((void *)(param_1 + 0x18));FUN_7ff78797f1f0(param_2,"_sale_id",param_1 + 0x18);
        << result::32-little, rest::binary>> = data
        {sale_id, ""} = Proto.read_byteArray rest
        %{result: result, sale_id: sale_id}
    end


    def parse(:RsRetrieveBMShopDisplayGoods, data) do
        <<
        flags,
        result :: 32-little,
        version:: 32-little,
        country_code_size,
        country_code::binary-size(country_code_size),
        rest::binary>> =  data

        {goods, rest} = if Bitwise.band(flags, 1) != 0 do
                Packet.readlist rest, fn(rest)->
                <<
                goods_id :: 32-little,
                item_id:: 32-little,
                item_count:: 64-little,
                rest::binary>> = rest

                {{goods_id, item_id, item_count}, rest}
            end
        else
            {[], rest}
        end

        {purchase_limit, rest} = if Bitwise.band(flags, 2) != 0 do
                Packet.readlist rest, fn(rest)->
                <<
                purchase_limit_id :: 32-little,
                limit_count::64-little,
                limit_duration_start::64-little,
                limit_duration_end::64-little,
                limit_target_type,
                limit_duration_type,
                rest::binary>> = rest
                {limit_desc, rest} = Proto.read_byteArray rest

                {
                    {purchase_limit_id, limit_count, limit_duration_start,
                    limit_duration_end, limit_target_type, limit_duration_type,
                    limit_desc
                }, rest}
            end
        else
            {[], rest}
        end

        {sale_goods, rest} = if Bitwise.band(flags, 4) != 0 do
            Packet.readlist rest, fn(rest)->
                <<
                sale_goods_id::32-little,
                fk_goods_id::32-little,
                fk_purchase_limit_id::32-little,
                base_sale_price::64-little,
                sale_price::64-little,
                mileage_point::64-little,
                purchasable_character_level::16-little,
                np_goods_id::32-little,
                rest::binary >> = rest
                {google_goods_id, rest} = Proto.read_byteArray rest
                {appstore_goods_id, rest} = Proto.read_byteArray rest
                {onestore_goods_id, rest} = Proto.read_byteArray rest
                {display_goods_name, rest} = Proto.read_byteArray rest
                {display_goods_description, rest} = Proto.read_byteArray rest
                {display_goods_icon, rest} = Proto.read_byteArray rest
                {event_description, rest} = Proto.read_byteArray rest
               <<sale_price_money_type,
               on_sale,
               event_label,
               highlight_edge,
               rest::binary
               >> = rest
               {
                   %{
                       sale_goods_id: sale_goods_id,
                       fk_goods_id: fk_goods_id,
                       fk_purchase_limit_id: fk_purchase_limit_id,
                       base_sale_price: base_sale_price,
                       sale_price: sale_price,
                       mileage_point: mileage_point,
                       purchasable_character_level: purchasable_character_level,
                       np_goods_id: np_goods_id,
                       google_goods_id: google_goods_id,
                       appstore_goods_id: appstore_goods_id,
                       onestore_goods_id: onestore_goods_id,
                       display_goods_name: display_goods_name,
                       display_goods_description: display_goods_description,
                       display_goods_icon: display_goods_icon,
                       event_description: event_description,
                       on_sale: on_sale,
                       event_label: event_label,
                       highlight_edge: highlight_edge,

                   }, rest
               }
            end
        else
            {[], rest}
        end


        {shop_category, rest} = if Bitwise.band(flags, 8) != 0 do
            Packet.readlist rest, fn(rest)->
                <<
                shop_category_id::32-little,
                list_order::32-little,
                rest::binary
                >> = rest

                {display_name, rest} = Proto.read_byteArray rest

                <<
                display_type,
                rest::binary
                >> = rest


                {
                    {shop_category_id, list_order, display_name, display_type},
                rest}
            end
        else
            {[], rest}
        end


        {shop, rest} = if Bitwise.band(flags, 16) != 0 do
            Packet.readlist rest, fn(rest)->
                <<
                shop_id::32-little,
                fk_shop_category_id::32-little,
                list_order::32-little,
                rest::binary
                >> = rest

                {display_name, rest} = Proto.read_byteArray rest

                <<
                event_label,
                rest::binary
                >> = rest


                {
                    {shop_id, fk_shop_category_id,
                    list_order, display_name, event_label},
                rest}
            end
        else
            {[], rest}
        end

        {display_goods, rest} = if Bitwise.band(flags, 32) != 0 do
            Packet.readlist rest, fn(rest)->
                <<
                display_goods_id::32-little,
                fk_shop_id::32-little,
                fk_sale_goods_id::32-little,
                list_order::32-little,
                display_start_time::64-little,
                display_end_time::64-little,
                sale_start_time::64-little,
                sale_end_time::64-little,
                sale_duration_type,
                rest::binary
                >> = rest

                {
                    {
                        display_goods_id,
                        fk_shop_id,
                        fk_sale_goods_id,
                        list_order,
                        display_start_time,
                        display_end_time,
                        sale_start_time,
                        sale_end_time,
                        sale_duration_type
                    },
                rest}
            end
        else
            {[], rest}
        end

        %{
            result: result,
            version: version,
            goods: goods,
            purchase_limit: purchase_limit,
            sale_goods: sale_goods,
            shop_category: shop_category,
            shop: shop,
            display_goods: display_goods
        }
    end

    def parse(:RqTradeCancelSaleTake, data) do
        # FUN_7ff78797f1f0(param_2,"_sale_id",param_1 + 0x10);
        # FUN_7ff7879b4170(param_2,"_sale_item_id",(longlong *)(param_1 + 0x20));
        # FUN_7ff78797f5e0(param_2,"_sale_item_count",param_1 + 0x28);
        # FUN_7ff7879bcc60(param_2,"_expired",param_1 + 0x30);

        # read_bytearray((void *)(param_1 + 0x10));
        # read_uint32((int *)(param_1 + 0x20));
        # read_qword(param_1 + 0x28);
        # 0x30 = readbyte()
        {sale_id, rest} = Proto.read_byteArray data
        <<sale_item_id::32-little, sale_item_count::64-little, expired>> = rest
        %{sale_id: sale_id, sale_item_id: sale_item_id, sale_item_count: sale_item_count, expired: expired}
    end

    def parse(:RsTradeCancelSaleTake, data) do
        <<result::32-little, rest::binary>> = data
        {sale_id, <<expired>>} = Proto.read_byteArray rest
        %{result: result, sale_id: sale_id, expired: expired}
    end


    def parse(:RqTradePurchaseTake, data) do
        # read_bytearray((void *)(param_1 + 0x10));FUN_7ff78797f1f0(param_2,"_sale_id",param_1 + 0x10);
        # {sale_id, rest} = Proto.read_byteArray rest
        # read_uint32((int *)(param_1 + 0x20));FUN_7ff7879b4170(param_2,"_sale_item_id",(longlong *)(param_1 + 0x20));
        # read_qword(param_1 + 0x28);FUN_7ff78797f5e0(param_2,"_sale_item_count",param_1 + 0x28);
        {sale_id, rest} = Proto.read_byteArray data
        <<sale_item_id::32-little, sale_item_count::64-little>> = rest
        %{sale_id: sale_id, sale_item_id: sale_item_id, sale_item_count: sale_item_count}
    end


    def parse(:RsTradePurchaseTake, data) do
        # read_uint32((int *)(param_1 + 0x10)); FUN_7ff7879bb770(param_2,"_result",param_1 + 0x10);
        # read_bytearray((void *)(param_1 + 0x18)); FUN_7ff78797f1f0(param_2,"_sale_id",param_1 + 0x18);
        << result::32-little, rest::binary>> = data
        {sale_id, ""} = Proto.read_byteArray rest
        %{result: result, sale_id: sale_id}
    end


    def parse(:RqTradeEarningsTake, data) do
        #array[varint] of bytearray // "_delivery_ids"
        {delivery_ids, ""} = readlist data, fn(rest)->
            {delivery_id, rest} = Proto.read_byteArray rest
        end
        %{delivery_ids: delivery_ids}
    end

    def parse(:RsTradeEarningsTake, data) do
        <<result::32-little>> = data
        %{target_key: result}
    end

    def parse(:NtTradeSoldItem, data) do
        {sale_id, rest} = Proto.read_byteArray data
        <<sold_item_id::32-little, sold_item_count::64-little>> = rest
        %{sale_id: sale_id, sold_item_id: sold_item_id, sold_item_count: sold_item_count}
    end

    #these are already defined, these are more correct, but breaks handlers
    #def parse(:NtFODisappear, data) do
    #    {key, rest} = Proto.varint_read data
    #    <<state, reason>> = rest
    #    %{key: key, state: state, reason: reason}
    #end
    #def parse(:RqFOInteraction, data) do
    #    <<target_key::32-little>> = data
    #    %{target_key: target_key}
    #end
    #def parse(:RsFOInteraction, data) do
    #    <<result::32-little, target_key::32-little>> = data
    #    %{result: result, id: target_key, target_key: target_key}
    #end
    #def parse(:NtFOInteractionStarted, data) do
    #    {key, rest} = Proto.varint_read data
    #    {interactor, ""} = Proto.varint_read rest
    #    %{key: key, interactor: interactor}
    #end
    #def parse(:NtFOInteractionInterrupted, data) do
    #    {key, rest} = Proto.varint_read data
    #    {interactor, ""} = Proto.varint_read rest
    #    %{key: key, interactor: interactor}
    #end
    #def parse(:NtFOInteractionCompleted, data) do
    #    {key, rest} = Proto.varint_read data
    #    {interactor, ""} = Proto.varint_read rest
    #    %{key: key, interactor: interactor}
    #end
    #def parse(:NtFODie, data) do
    #    {key, ""} = Proto.varint_read data
    #    %{key: key}
    #end
    #def parse(:NtFOSwitchReset, data) do
    #    {key, ""} = Proto.varint_read data
    #    %{key: key}
    #end

    def parse(:NtSkillCastBegin, data) do
        #<<161, 173, 4,
        #241, 112,
        #213, 244, 153, 200,
        #184, 79, 46, 72,
        #66, 38, 153, 69,
        #235, 21, 0, 0, 3, 0, 235, 43, 206, 206, 198, 243, 1, 144, 78, 0, 0, 0>>
        {char_id, data} = Proto.varint_read data
        {target_id, _} = Proto.varint_read data
        %{char_id: char_id, target_id: target_id}
    end
    #<<241, 112, 161, 22, 1, 0, 0, 0, 0, 0>>, %{char_id: 14449, monster_id: 71329}}
    def parse(:NtSkillCastEnd, data) do
        #<<161, 173, 4, 235, 21, 0, 0, 0, 0>>
        {char_id, <<skill_id::32-little, rest::binary>>} = Proto.varint_read data
        %{char_id: char_id, skill_id: skill_id, rest: rest}
    end

    def parse(:RqCraftItem, data) do
        data
    end

    def parse(:RsCraftItem, data) do
        {results, rest} = readlist data, fn(<<result::32-little, rest::binary>>)->
            {result, rest}
        end
        <<craft_id::32-little,
        craft_cnt::32-little>> = rest
        %{results: results, craft_id: craft_id, craft_cnt: craft_cnt}
    end

    #def parse(:NtMonsterAppear, data) do
    #    %{bin: data}
    #end
    def parse(:NtMonsterAppear, data) do
        #"check re/monster.md"
        {id, rest} = Proto.varint_read data
        <<flags0, rest :: binary>> = rest

        #description field, 0x18
        {has_nickname, rest} = Proto.read_byte rest
        {nickname, rest} = if has_nickname != 0 do
            Proto.read_byteArray rest
        else
            {nil, rest}
        end
        <<type::32-little, rest::binary>> = rest

        <<199, 186, 4,
        2, 1, 15, 235, 130, 160, 236, 156, 188, 235, 138, 148, 234, 176,
          156, 235, 175, 184, 3, 117, 2, 0, 1, 93, 105, 127, 70, 101, 197, 46, 197, 80,
          42, 172, 69, 117, 1, 236, 1, 236, 1, 25, 25, 241, 255, 0, 1, 1, 1, 1>>

<<250, 196, 1,
4, 0,
193, 138, 1, 0,

1, 128, 97, 87, 200, 64, 173, 87, 72, 199,
  93, 200, 69, 233, 1, 209, 1, 209, 1, 25, 25, 241, 255, 0, 1, 1, 6, 0>>

        #rest = if Bitwise.band(flags0, 1) != 0 do
        #    throw FUN_7ff7879bd8b0
        #else rest end
        #rest = if Bitwise.band(flags0, 2) == 0 do
        #    rest
        #else throw("((bVar3 & 2) == 0)") end
        #rest = if Bitwise.band(flags0, 4) == 0 do
        #    rest
        #else
        #    <<_ukn,rest::binary>> = rest
        #    rest
        #end

        <<flags2,
        x::float-32-little, y::float-32-little, z::float-32-little,
        dir,
        live,
        rest::binary>> = rest

        {hp_cur, rest} = Proto.varint_read rest
        {hp_max, rest} = Proto.varint_read rest
        {mp_cur, rest} = Proto.varint_read rest
        {mp_max, rest} = Proto.varint_read rest

        {karma, rest} = if (Bitwise.band(flags2,1) != 0) do
            <<k::16-little, rest::binary>> = rest
            {k, rest}
        else
            {0, rest}
        end
        {char_state, rest} = if (Bitwise.band(flags2,2) != 0) do
            <<k, rest::binary>> = rest
            {k, rest}
        else
            {0, rest}
        end
        {toggle_flags, rest} = if (Bitwise.band(flags2,4) != 0) do
            <<k::32-little, rest::binary>> = rest
            {k, rest}
        else
            {0, rest}
        end
        {stat_move_speed, rest} = Proto.varint_read rest
        {phase, rest} = if (Bitwise.band(flags2,8) != 0) do
            <<k, rest::binary>> = rest
            {k, rest}
        else
            {0, rest}
        end
        {seq_frame, rest} = if (Bitwise.band(flags2,0x10) != 0) do
            <<k::16-little, rest::binary>> = rest
            {k, rest}
        else
            {0, rest}
        end
        {seq_idx, rest} = if (Bitwise.band(flags2,0x20) != 0) do
            <<k, rest::binary>> = rest
            {k, rest}
        else
            {0, rest}
        end
        <<relationship_case, rest::binary>> = rest

        #continuation after reading state

        # if ((bVar3 & 1) != 0) {
        #   FUN_7ff7879bd8b0(param_1 + 0x88); FUN_7ff7879b4990(param_2,"_abnormals",param_1 + 0x88);
        # }
        {abnormals, rest} = if (Bitwise.band(flags0,1) != 0) do
            {abnormals, rest} = readlist rest, fn(x)->
                parse_abnormal x
            end
            {abnormals, rest}
        else
            {[], rest}
        end
        # if ((bVar3 & 2) == 0) {
        #   *(undefined *)(param_1 + 0xa0) = 0;
        # }
        # else {
        #   readbyte(param_1 + 0xa0); FUN_7ff7879b3b00(param_2,"_moving",param_1 + 0xa0);
        # }
        {moving, rest} = if (Bitwise.band(flags0,2) != 0) do
            Proto.read_byte rest
        else
            {0, rest}
        end
        # if ((bVar3 & 4) == 0) {
        #   *(undefined *)(param_1 + 0xa1) = 0;
        # }
        # else {
        #   readbyte(param_1 + 0xa1); FUN_7ff7879b3b00(param_2,"_running",param_1 + 0xa1);
        # }
        {running, rest} = if (Bitwise.band(flags0,4) != 0) do
            Proto.read_byte rest
        else
            {0, rest}
        end

        # *(undefined *)(param_1 + 0xa2) = readbyte FUN_7ff7879b6ff0(param_2,"_reason",param_1 + 0xa2);
        # *(undefined *)(param_1 + 0xa3) = readbyte FUN_7ff7879bcc60(param_2,"_aggressive",param_1 + 0xa3);
        {reason, rest} = Proto.read_byte rest
        {aggresive, rest} = Proto.read_byte rest

        # if ((bVar3 & 8) == 0) {
        #   *(undefined4 *)(param_1 + 0xa4) = 0;
        # }
        # else {
        #   read_uint32((int *)(param_1 + 0xa4)); FUN_7ff7879b4640(param_2,"_occupied_key",param_1 + 0xa4);
        # }
        {occupied_key, rest} = if (Bitwise.band(flags0,8) != 0) do
            <<key::32-little>> = rest
            {key, ""}
        else
            {0, rest}
        end

        %{id: id, type: type,
        nickname: nickname,
        flags0: flags0,
        pos: {trunc(x), trunc(y), trunc(z)},

        live: live,
        hp_cur: hp_cur,
        hp_max: hp_max,
        mp_cur: mp_cur,
        mp_max: mp_max,
        char_state: char_state,
        stat_move_speed: stat_move_speed,
        phase: phase,
        abnormals: abnormals,
        running: running,
        moving: moving,
        reason: reason,
        aggresive: aggresive,
        occupied_key: occupied_key
        }
    end

    def parse(:NtCharAppear, data) do
        {key, rest} = Proto.varint_read data
        <<flags::16-little, rest :: binary>> = rest
        #desc
        <<has_nickname, rest :: binary >> = rest
        {nickname, rest} = if has_nickname != 0 do
          {nn, rest} = Proto.read_byteArray rest
          {nn, rest}
        else
          {"", rest}
        end
        <<pc_id::32-little, rest::binary>> = rest

        #state
        <<state_flags,
          x::32-little-float,
          y::32-little-float,
          z::32-little-float, rest:: binary>> = rest

        len = byte_size(rest) - 8 - if Bitwise.band(flags, 0x100) do
          1
        else
          0
        end

        <<blob :: binary-size(len), dbid :: 64-little, statue_mode>> = rest
        %{id: key, nickname: nickname,
            pc_id: pc_id, dbid: dbid, pos: {trunc(x),trunc(y),trunc(z)},
            blob: blob,
            flags: flags, state_flags: state_flags, statue_mode: statue_mode}
    end
    def parse(:NtCharDisappear, data) do
        {id, <<type>>} = Proto.varint_read data
        %{id: id, type: type}
    end

    def parse(:NtMonsterDisappear, data) do
        {id, <<reason>>} = Proto.varint_read data
        %{id: id, reason: reason}
    end

    def parse(:NtMoveSpeedUpdate, data) do
        {id, rest} = Proto.varint_read data
        {speed, ""} = Proto.varint_read rest
        %{id: id, speed: speed}
    end

    def parse(:NtCharDie, data) do
        #{:server, :NtCharDie, <<164, 177, 3, 23, 38, 0, 0, 7, 97, 114, 99, 104, 101, 114, 49, 0, 0, 0, 0, 120, 5, 0, 0>>}
        {id, _} = Proto.varint_read data

          # read_varint((void *)(param_1 + 0x10));
          # read_uint32((int *)(param_1 + 0x18)); //FUN_7ff7879b4640(param_2,"_caster_key",param_1 + 0x18);
          # read_bytearray((void *)(param_1 + 0x20));//FUN_7ff78797f1f0(param_2,"_pc_caster_nickname",param_1 + 0x20);
          # read_uint32((int *)(param_1 + 0x30));//FUN_7ff7879b4170(param_2,"_monster_caster_data_id",param_1 + 0x30);
          # read_uint32((int *)(param_1 + 0x34));//FUN_7ff7879b4170(param_2,"_skill_id",param_1 + 0x34);

        %{id: id}
    end

    def parse(:RqSkillCast, data) do
        {key, <<skill_id::32-little, target_key::32-little, parts_id::32-little>>} = Proto.varint_read data
        %{key: key, skill_id: skill_id, target_key: target_key, parts_id: parts_id}
    end

    def parse(:RsSkillCast, data) do
        #{:server, :RsSkillCast, <<221, 61, 9, 24, 0, 0, 120, 5, 0, 0, 198, 218, 4, 0>>}
        {key, <<result::32-little, skill_id::32-little, rest::binary>>} = Proto.varint_read data
        {target_key, rest} = Proto.varint_read rest
        {parts_id, rest} = Proto.varint_read rest
        %{char_id: key, error: result, skill_id: skill_id, target_key: target_key, parts_id: parts_id}
    end

    def parse(:NtBasicAtkToggleSwitch, rest) do
        #{:server, :NtBasicAtkToggleSwitch, <<234, 49, 1, 162, 212, 3, 0, 1, 0, 0, 0>>  #started
        #{:server, :NtBasicAtkToggleSwitch, <<234, 49, 0, 0, 0, 4, 0, 0, 0>>  #mob died
        #{:server, :NtBasicAtkToggleSwitch, <<234, 49, 0, 0, 0, 1, 0, 0, 0>>  #canceled
        #{:server, :NtBasicAtkToggleSwitch, <<149, 69, 1, 148, 194, 4, 0, 1, 0, 0, 0>>} #out of range?

        {char_id, <<is_on, rest::binary>>} = Proto.varint_read rest
        {target_key, rest} = Proto.varint_read rest
        {target_parts_id, <<reason::32-little>>} = Proto.varint_read rest
        %{char_id: char_id, target_id: target_key, reason: reason}
    end
    def parse(:NtMonsterFirstAggro, rest) do
        #{:server, :NtMonsterFirstAggro, <<162, 212, 3, 234, 49>>
        {id, rest} = Proto.varint_read rest
        {char_id, rest} = Proto.varint_read rest
        %{char_id: char_id, id: id}
    end
    def parse(:NtMonsterOccupied, rest) do
        #{:server, :NtMonsterOccupied, <<162, 212, 3, 234, 49>>
        {id, rest} = Proto.varint_read rest
        {char_id, rest} = Proto.varint_read rest
        %{char_id: char_id, id: id}
    end


    def parse(:RqBasicAtkToggleOn, data) do
        {id, rest} = Proto.varint_read data
        <<monster_id::64-little>> = rest
        %{char_id: id, monster_id: monster_id}
    end

    def parse(:RsCharMoveStop, data) do
        {id, rest} = Proto.varint_read data
        <<x::float-32-little,y::float-32-little,z::float-32-little,
            running, dir::32-little>> = rest
        %{char_id: id, pos: {trunc(x),trunc(y),trunc(z)}, running: running, dir: dir}
    end
    def parse(:RsCharMoveStart, data) do
        {id, rest} = Proto.varint_read data
        <<x::float-32-little,y::float-32-little,z::float-32-little,
            running, dir::16-little>> = rest
        %{char_id: id, pos: {trunc(x),trunc(y),trunc(z)}, running: running, dir: dir}
    end
    def parse(:RsCharMove, data) do
        {id, rest} = Proto.varint_read data
        <<x::float-32-little,y::float-32-little,z::float-32-little,
            running, dir::16-little>> = rest
        %{char_id: id, pos: {trunc(x),trunc(y),trunc(z)}, running: running, dir: dir}
    end
    def parse(:NtUpdateZone, data) do
        {key, rest} = Proto.varint_read data
        <<zone_id::32-little, map_id::32-little>> = rest
        %{key: key, zone_id: zone_id, map_id: map_id}
    end
    def parse(:RsCharMoveOffset, data) do
        {char_id, rest} = Proto.varint_read data
        <<x,y,z,running,dir::16>> = rest
        %{char_id: char_id, running: running, dir: dir, pos: {trunc(x),trunc(y),trunc(z)}}
    end

    def parse(:RqCharDir, data) do
        {id, rest} = Proto.varint_read data
        <<angle::32-little-float>> = rest
        %{char_id: id, angle: angle}
    end

    def parse(:RqCharMoveStart, data) do
        {char_id, rest} = Proto.varint_read data
        <<x::float-32-little,y::float-32-little,z::float-32-little,
        x1::float-32-little,y1::float-32-little,z1::float-32-little>> = rest
        %{char_id: char_id, source: {x,y,z}, delta: {x1,y1,z1}}
    end
    def parse(:RqCharMove, data) do
        {char_id, rest} = Proto.varint_read data
        <<x::float-32-little,y::float-32-little,z::float-32-little,
        x1::float-32-little,y1::float-32-little,z1::float-32-little>> = rest
        %{char_id: char_id, source: {x,y,z}, delta: {x1,y1,z1}}
    end
    def parse(:RqCharMoveStop, data) do
        {char_id, rest} = Proto.varint_read data
        <<x::float-32-little,y::float-32-little,z::float-32-little>> = rest
        %{char_id: char_id, pos: {trunc(x),trunc(y),trunc(z)}}
    end

    def parse(:RsSelectCharacter, buf) do
        <<char_id::little-32, error::little-32, _::binary>> = buf
        %{char_id: char_id, error: error}
    end

    def parse(:"HeartBeat", buf) do
        <<time_bin::binary>> = buf
        %{time_bin: time_bin}
    end

    def parse(:"NtHpUpdate", buf) do
        #{:server, :NtHpUpdate, <<140, 63, 245, 1>>}
        {char_id, rest} = Proto.varint_read buf
        {hp_cur,_} = Proto.varint_read rest
        %{char_id: char_id, hp_cur: hp_cur}
    end
    def parse(:"NtMpUpdate", buf) do
        #{:server, :NtMpUpdate, <<140, 63, 162, 4>>}
        {char_id, rest} = Proto.varint_read buf
        {mp_cur,_} = Proto.varint_read rest
        %{char_id: char_id, mp_cur: mp_cur}
    end
    def parse(:"NtUpdateVitality", buf) do
        {vitality,_} = Proto.varint_read buf
        %{vitality: vitality}
    end

    def parse(:NtPrepareEnterMap, buf) do
        #{:server, :NtPrepareEnterMap, <<92, 0, 0, 0, 192, 101, 107, 200, 0, 234, 65, 72, 128, 217, 169, 69, 63, 14, 62, 193, 0, 0, 0, 0, 0>>}
        <<map_id::32-little, x::32-little-float, y::32-little-float, z::32-little-float,
            angle::32-little-float, rest::binary>> = buf
        %{map_id: map_id, pos: {trunc(x),trunc(y),trunc(z)}, angle: angle, rest: rest}
    end
    def parse(:RsSkillList, rest) do
        #prob like shoot, loot, radar
        #{:server, :RsSkillList, <<4, 50, 0, 0, 0, 0, 120, 5, 0, 0, 0, 125, 5, 0, 0, 0, 193, 168, 50, 0, 0>>}

        <<count, rest::binary>> = rest
        itr = :lists.seq(0, count-1)
        {rest, skills} = Enum.reduce itr, {rest, []}, fn(_, {rest, acc})->
            <<type::32-little, rest::binary>> = rest
            {cd,rest} = Proto.varint_read rest
            {auto_cd,rest} = Proto.varint_read rest

            skill = Map.merge(%{type: type, cd: cd, auto_cd: auto_cd}, Type.skill(type))
            {rest, acc ++ [skill]}
        end
        %{skills: skills}
    end

    def parse(:RsAllStat, <<count, buf::binary>>) do
        #{:server, :RsAllStat, <<35,
        #1, 14, 2, 18, 3, 16, 4, 13, 5, 18, 6, 16, 7, 231, 1, 8, 3,
        #9, 160, 237, 173, 18,
        #11, 2, 12, 6, 13, 5, 14, 5, 16, 249, 3, 17, 6, 21, 1, 23, 152, 17,
        #25, 232, 7,
        #29, 2, 55, 4, 58, 6, 59, 8, 62, 5, 63, 5, 64, 4, 65, 6, 66, 6, 67, 6, 71, 3, 72, 2, 76, 6, 138, 6, 141, 6, 157, 4, 158, 6>>}

        #<<_, 1, str, 2, dex_agi, 3, int_wis, 4, con, 5, dex_agi_2, 6, wis_int_2, _::binary>> = buf

        {rest, stats} = Enum.reduce 1..count, {buf, []}, fn(_, {buf, acc})->
            <<type, rest::binary>> = buf
            {v,rest} = Proto.varint_read rest
            {rest, acc ++ [{type, v}]}
        end

        map = Enum.reduce(stats, %{}, fn({type, v}, acc)->
            type = case type do
                1 -> :str
                2 -> :dex
                3 -> :int
                4 -> :con
                5 -> :agi
                6 -> :wis
                7 -> :hp_max
                9 -> :weight
                11 -> :ac
                16 -> :mp_max
                17 -> :mp_regen
                19 -> :all_hit
                20 -> :bonus_damage
                21 -> :weapon_damage
                22 -> :all_damage
                23 -> :attack_speed
                24 -> :move_speed
                25 -> :cast_speed
                34 -> :shadow_damage
                35 -> :holy_damage
                55 -> :arrow_damage
                60 -> :attack_range
                61 -> :double_damage
                65 -> :ranged_damage
                66 -> :ranged_hit
                67 -> :ranged_crit
                69 -> :melee_crit_resist
                88 -> :crit_damage
                128 -> :mp_regen_pot
                _ -> type
            end
            v = if type == :move_speed do
                20
                v
            else v end
            Map.put(acc, type, v)
        end)

        #Serializable
        Map.merge(%{stats: stats}, %{stat_map: map})
    end


    def parse(:NtCharStateUpdate, data) do
        {char_id, state} = Proto.varint_read data
        %{char_id: char_id, state: state}
    end

    def parse(:NtStates, <<count, buf::binary>>) do
        #{:server, :NtStates, <<0>>}
        #hasted
        #{:server, :NtStates, <<2,     23, 220, 36,   24, 196, 19>>}
        #{:server, :NtStates, <<2,     7, 142, 2,     16, 254, 3>>}

        itr = :lists.seq(0, count-1)
        {"", stats} = Enum.reduce itr, {buf, []}, fn(_, {buf, acc})->
            <<type, rest::binary>> = buf
            {v,rest} = Proto.varint_read rest
            {rest, acc ++ [{type, v}]}
        end

        map = Enum.reduce(stats, %{}, fn({type, v}, acc)->
            type = case type do
                1 -> :str
                2 -> :dex
                3 -> :int
                4 -> :con
                5 -> :agi
                6 -> :wis
                7 -> :hp_max
                9 -> :weight
                11 -> :ac
                16 -> :mp_max
                17 -> :mp_regen
                19 -> :all_hit
                20 -> :bonus_damage
                21 -> :weapon_damage
                22 -> :all_damage
                23 -> :attack_speed
                24 -> :move_speed
                25 -> :cast_speed
                34 -> :shadow_damage
                35 -> :holy_damage
                55 -> :arrow_damage
                60 -> :attack_range
                61 -> :double_damage
                65 -> :ranged_damage
                66 -> :ranged_hit
                67 -> :ranged_crit
                69 -> :melee_crit_resist
                88 -> :crit_damage
                128 -> :mp_regen_pot
                _ -> type
            end
            v = if type == :move_speed do
                20
                v
            else v end
            Map.put(acc, type, v)
        end)

        %{stats: stats, stat_map: map}
    end

    def readlist(rest, afun) do
        {count, rest} = Proto.varint_read rest
        if count > 0 do
            Enum.reduce 1..count, {[], rest}, fn(_, {acc, rest})->
                {item, rest} = afun.(rest)
                {acc ++ [item], rest}
            end
        else
            {[], rest}
        end
    end

    def parse(:RsQuestList, b) do
        #{:server, :RsQuestList, <<1, 132, 29, 154, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0>>}
        {quests, rest} = readlist b, fn(rest)->
            <<quest_uuid::32-little,
            status, rest::binary>> = rest
            {step_order, rest} = Proto.varint_read rest
            {objective1, rest} = Proto.varint_read rest
            {objective2, rest} = Proto.varint_read rest
            {objective3, rest} = Proto.varint_read rest
            <<quest_id::32-little, rest::binary>> = rest #FUN_7ff7879b4170(param_2,"_uid",param_1 + 0x28);
            {vi_0x2c, rest} = Proto.varint_read rest #FUN_7ff7879b4170(param_2,"_roll_count",param_1 + 0x2c);
            <<ui_0x30::32-little, rest::binary>> = rest #FUN_7ff7879b4170(param_2,"_trophy_id",param_1 + 0x30);
            {alist, rest} = readlist rest, fn(rest)-> #_random_reward_items
                <<a::32-little, b::32-little, rest::binary>> = rest
                {{a, b}, rest}
            end
            #{quest_uuid, status, step_order, step_order2, obj, obj2, quest_id}
            v = %{quest_uuid: quest_uuid, status: status, quest_step: step_order, obj_0: objective1,
                obj_1: objective2, obj_2: objective3, id: quest_id}
            v = Map.merge(v, Type.quest(quest_uuid))
            {v, rest}
        end
        {life_quests_completed, ""} = readlist rest, fn(rest)->
            <<
            id::32-little,
            rest::binary
            >> = rest
            {id, rest}
        end
        life_quests_completed = Enum.into(
            life_quests_completed, %{}, fn(id)-> {id,id} end)
        # void FUN_7ff7879de660(longlong param_1)
        #
        # {
        #
        #   read_uint32((int *)(param_1 + 0x10));
        #   *(undefined *)(param_1 + 0x14) = readbyte()
        #   read_varint2((void *)(param_1 + 0x18));
        #   read_varint2((void *)(param_1 + 0x1c));
        #   read_varint2((void *)(param_1 + 0x20));
        #   read_varint2((void *)(param_1 + 0x24));
        #   read_uint32((int *)(param_1 + 0x28));
        #   read_varint2((void *)(param_1 + 0x2c));
        #   read_uint32((int *)(param_1 + 0x30));
        #   FUN_7ff7879c7da0(param_1 + 0x38);
        #   return;
        # }
        #

        # void FUN_7ff7879db6d0(longlong param_1)
        #
        # {
        #   read_uint32((int *)(param_1 + 0x10));
        #   read_uint32((int *)(param_1 + 0x14));
        #   return;
        # }
        #

        #Serializable
        %{quests: quests, life_quests_completed: life_quests_completed}
    end
    def parse(:NtQuestAdd, buf) do
        #first quest
        #{:server, :NtQuestAdd, <<132, 29, 154, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0>>}
        #second quest
        #{:server, :NtQuestAdd, <<142, 29, 154, 0, 1, 1, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0>>}
        #third quest
        #{:server, :NtQuestAdd, <<232, 29, 154, 0, 1, 1, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0>>}

        <<quest_uuid::32-little, status, rest::binary>> = buf
        {step_order, rest} = Proto.varint_read rest
        {step_order2, rest} = Proto.varint_read rest
        {objective, rest} = Proto.varint_read rest
        {objective2, rest} = Proto.varint_read rest
        <<quest_id::32-little, rest::binary>> = rest

        #read_uint32((int *)(param_1 + 0x10)); //FUN_7ff7879b4170(param_2,"_quest_id",param_1 + 0x10);
        #*(undefined *)(param_1 + 0x14) = read_byte() //FUN_7ff7879bb430(param_2,"_status",param_1 + 0x14);
        #read_varint2((void *)(param_1 + 0x18)); //FUN_7ff7879b4170(param_2,"_step_order",param_1 + 0x18);
        #read_varint2((void *)(param_1 + 0x1c)); //FUN_7ff7879b4170(param_2,"_step_order",param_1 + 0x18);
        #read_varint2((void *)(param_1 + 0x20)); //FUN_7ff7879b4170(param_2,"_objective2",param_1 + 0x20);
        #read_varint2((void *)(param_1 + 0x24)); //FUN_7ff7879b4170(param_2,"_objective3",param_1 + 0x24);
        #read_uint32((int *)(param_1 + 0x28)); //FUN_7ff7879b4170(param_2,"_uid",param_1 + 0x28);
        #read_varint2((void *)(param_1 + 0x2c)); //FUN_7ff7879b4170(param_2,"_roll_count",param_1 + 0x2c);
        #read_uint32((int *)(param_1 + 0x30)); //FUN_7ff7879b4170(param_2,"_trophy_id",param_1 + 0x30);
        #FUN_7ff7879c7da0(param_1 + 0x38); //FUN_7ff78703def0(acStack184,0x80,"%s:vec[size=%d]","_random_reward_items");

        v = %{quest_uuid: quest_uuid, status: status, quest_step: step_order, obj_0: step_order2,
            obj_1: objective, obj_2: objective2, id: quest_id}
        Map.merge(v, Type.quest(quest_uuid))
    end
    #TODO
    def parse(:NtQuestUpdateProgress, buf) do
        #{:server, :NtQuestUpdateProgress, <<1, 0, 0, 0, 2, 1, 1, 0, 0>>}
        #{:server, :NtQuestUpdateProgress, <<1, 0, 0, 0, 3, 1, 1, 0, 0>>}

        #haste potion
        #{:server, :NtQuestUpdateProgress, <<2, 0, 0, 0, 2, 1, 1, 0, 0>>}
        #{:server, :NtQuestUpdateProgress, <<2, 0, 0, 0, 2, 2, 0, 0, 0>>}
        #haste potion 2
        #{:server, :NtQuestUpdateProgress, <<2, 0, 0, 0, 2, 2, 1, 0, 0>>}
        #{:server, :NtQuestUpdateProgress, <<2, 0, 0, 0, 2, 3, 0, 0, 0>>}

        #haste potion end
        #{:server, :NtQuestUpdateProgress, <<2, 0, 0, 0, 2, 3, 1, 0, 0>>}
        #{:server, :NtQuestUpdateProgress, <<2, 0, 0, 0, 3, 3, 1, 0, 0>>}
        <<quest_id::32-little, status, rest::binary>> = buf
        {quest_step, rest} = Proto.varint_read rest
        {step_1, rest} = Proto.varint_read rest
        {step_2, rest} = Proto.varint_read rest
        {step_3, rest} = Proto.varint_read rest
        %{id: quest_id, status: status, quest_step: quest_step, obj_0: step_1,
            obj_1: step_2, obj_2: step_3
        }
    end
    #TODO
    def parse(:RsDungeonTimes, buf) do
        <<count, rest::binary >> = buf
        {rest, dungeon_times} =
          Enum.reduce 1..count, {rest, []}, fn( _iteration, {rest, acc}) ->
            <<map_group_id::32-little,
              remain_time::64-little,
              rest::binary >>        = rest
              updated_acc = acc ++ [
                %{ map_group_id: map_group_id,
                remain_time: remain_time}]
            {rest, updated_acc}
        end
        %{dungeon_times: dungeon_times, rest: rest}
    end
    def parse(:NtDungeonTimeUpdate, data) do
        <<id::32-little, time::64-little,_::binary>> = data
        %{map_group_id: id, remain_time: time}
    end

    def parse(:RsTelpo, buf) do
        #{:server, :RsTelpo, <<245, 171, 155, 0, 0, 0, 0, 0>>}
        %{bin: buf}
    end

    def parse(:RqCharRespawn, buf) do
        {id, <<>>} = Proto.varint_read buf
        %{id: id}
    end

    def parse(:RsItemUse, buf) do
        #{:server, :RsTelpo, <<245, 171, 155, 0, 0, 0, 0, 0>>}
        <<error::32-little, id::64-little>> = buf
        %{error: error, id: id}
    end
    def parse(:RsItemUseSelectBox, buf) do
        <<id::64-little, index, error::32-little>> = buf
        %{error: error, id: id, index: index}
    end

    def parse(:RsItemContainerList, buf) do
        <<
            result::32-little,
            extend_cnt,
            storage_extend_cnt,
            rest::binary>> = buf

        {items, equip_items, items_mapped, rest} = parse_item_struct(rest)
        %{result: result, items: items, equip_items: equip_items, items_mapped: items_mapped, rest: rest}
    end
    def parse(:NtItemInfoUpdate, buf) do
        #red potion
        #{:server, :NtItemInfoUpdate, <<1, 223, 130, 13, 0, 0, 92, 2, 0,
        #33, 130, 103, 30, 171, 2, 1, 223, 122, 20, 174, 71, 225, 122, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 22, 0>>}
        #scroll of escape
        #{:server, :NtItemInfoUpdate, <<1, 128, 207, 12, 0, 0, 113, 3, 0,
        #65, 35, 111, 30, 9, 1, 201, 204, 204, 204, 204, 204, 204, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 14, 0>>}
        #quiver equipped
        #{:server, :NtItemInfoUpdate, <<0, 1, 0, 9, 10, 0, 0, 8, 3, 0,
        #129, 139, 114, 24, 184, 23, 1, 234, 81, 184, 30, 133, 235, 81, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0>>}
        {items, equip_items, items_mapped, rest} = parse_item_struct(buf)
        %{items_mapped: items_mapped, res: rest}
    end
    def parse(:NtItemInfoAdd, buf) do
        #some reward coin
        #{:server, :NtItemInfoAdd, <<0, 2, 0, 0, 1, 187, 208, 10, 0, 0, 36, 5, 0, 1, 233, 164, 53, 232, 7, 1, 190, 245, 40, 92, 143, 194, 245, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 24, 0, 0, 0, 0, 0>>}
        <<error::32-little, rest::binary>> = buf
        {items, equip_items, items_mapped, rest} = parse_item_struct(rest)
        %{items_mapped: items_mapped, res: rest}
    end
    def parse(:NtItemInfoRemove, buf) do
        #{:server, :NtItemInfoRemove, <<1, 180, 60, 14, 0, 0, 230, 0, 0, 43, 63, 107, 30, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 14>>
        {items, equip_items, items_mapped, rest} = parse_item_struct(buf)
        %{items_mapped: items_mapped, res: rest}
    end

    def parse(:NtSkillAdd, buf) do
        #{:server, :NtSkillAdd, <<187, 71, 180, 8, 52, 0>>
        {char_id, <<skill_id::32-little>>} = Proto.varint_read buf
        %{char_id: char_id, skill_id: skill_id}
    end

    #These are interactables
    def parse(:NtFOAppear, buf) do
        #{:server, :NtFOAppear,
        #good = <<151, 174, 2, 0, 0, 67, 15, 0, 128, 227, 115, 200, 192, 129, 87, 72, 5, 152, 172, 69, 0, 48, 177, 66, 0, 1, 235, 129, 7, 49, 111, 1, 0, 0>>
        #bad = <<250, 196, 1, 4, 0, 193, 138, 1, 0, 1, 128, 97, 87, 200, 64, 173, 87, 72, 199, 93, 200, 69, 233, 1, 209, 1, 209, 1, 25, 25, 241, 255, 0, 1, 1, 6, 0>>
        {id, <<flag, rest::binary>>} = Proto.varint_read buf
        rest = case Bitwise.&&&(flag, 1) do
            1 ->
                {size, rest} = Proto.varint_read rest
                <<_nickname::binary-size(size), rest::binary>> = rest
                rest
            0 -> rest
        end

        <<type::32-little,
            x::32-float-little, y::32-float-little, z::32-float-little,
            dir::32-little, state, reason, create_time::little-64, _::binary
        >> = rest

        %{id: id, type: type,  pos: {trunc(x),trunc(y),trunc(z)}, dir: dir, state: state,
            reason: reason, create_time: create_time}
    end
    def parse(:NtFODisappear, buf) do
        #{:server, :NtFODisappear, <<142, 172, 4, 0, 1>>
        {id, _} = Proto.varint_read buf
        %{id: id}
    end
    def parse(:NtFODie, buf) do
        #{:server, :NtFODisappear, <<142, 172, 4, 0, 1>>
        {id, _} = Proto.varint_read buf
        %{id: id}
    end
    def parse(:RsFOInteraction, buf) do
        #{:server, :RsFOInteraction, <<0, 0, 0, 0, 165, 241, 0, 0>>
        <<error::32-little, id::32-little>> = buf
        %{id: id, error: error}
    end
    def parse(:NtFOInteractionStarted, buf) do
        #{:server, :NtFOInteractionStarted, <<165, 227, 3, 184, 40>>
        {id, rest} = Proto.varint_read buf
        {char_id, rest} = Proto.varint_read rest
        %{id: id, char_id: char_id}
    end
    def parse(:NtFOInteractionCompleted, buf) do
        #{:server, :NtFOInteractionCompleted, <<165, 227, 3, 184, 40>>
        {id, rest} = Proto.varint_read buf
        {char_id, rest} = Proto.varint_read rest
        %{id: id, char_id: char_id}
    end
    def parse(:NtFOSwitchReset, buf) do
        #{:server, :NtFOSwitchReset, <<165, 227, 3>>
        {id, _} = Proto.varint_read buf
        %{id: id}
    end

    def parse(:NtDropItem, rest) do
        #:NtDropItem,
        # <<151, 197, 2, 241, 157, 138, 198, 225, 135, 171, 70, 3, 174, 59, 69,
        #162, 143, 12, 0, 0, 2, 2, 0,
        #14, 51, 238, 11,
        #1, 0, 0, 0,
        #92, 123, 138, 198, 93, 192, 172, 70, 2, 174, 59, 69,
        #2,
        #198, 117, 0, 0, 0, 0, 104, 0,
        #227, 253, 140, 72, 111, 1, 0, 0>>}

        {char_id, rest} = Proto.varint_read rest
        <<char_x::32-little-float, char_y::32-little-float, char_z::32-little-float, rest::binary>> = rest
        <<id::64-little, type::32-little, amount::32-little,
            x::32-little-float, y::32-little-float, z::32-little-float,
            owner_type, owner::64-little, owner_expire_time::64-little>> = rest

        %{char_id: char_id, id: id, type: type, amount: amount, owner_type: owner_type,
            owner: owner, owner_expire_time: owner_expire_time, pos: {trunc(x),trunc(y),trunc(z)}}
    end
    def parse(:NtDropItemRemove, buf) do
        #%{_op: :NtDropItemRemove, bin: <<139, 161, 2, 247, 101, 14, 0, 0, 46, 0, 0>>}}
        {char_id, <<id::64-little>>} = Proto.varint_read buf
        %{char_id: char_id, id: id}
    end
    def parse(:RsGetDropItem, buf) do
        #{:server, :RsGetDropItem, <<29, 180, 12, 0, 0, 89, 1, 0, 0, 0, 0, 0>>
        <<id::64-little, error::32-little>> = buf
        %{id: id, error: error}
    end

    def parse(:RsNpcTouch, buf) do
        #{:server, :RsNpcTouch, <<43, 33, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0>>}
        #out of range or not exist
        #{:ignored, %{_op: :RsNpcTouch, error: 4913, npc_id: 85847}}
        <<npc_id::64-little, error::32-little>> = buf
        %{id: npc_id, error: error}
    end

    def parse(:RsQuestTakeReward, buf) do
        #{:server, :RsQuestTakeReward, <<1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0>>}
        <<quest_id::32-little, _::binary>> = buf
        %{id: quest_id}
    end
    def parse(:RsQuestAccept, buf) do
        #{:server, :RsQuestAccept, <<6, 0, 0, 0, 0, 0, 0, 0>>}
        <<quest_id::32-little, _::binary>> = buf
        %{id: quest_id}
    end
    def parse(:RsGuideQuestAccept, buf) do
        #{:server, :RsQuestAccept, <<6, 0, 0, 0, 0, 0, 0, 0>>}
        <<quest_id::32-little, error::32-little>> = buf
        %{id: quest_id, error: error}
    end
    def parse(:RsQuestObjectiveComplete, buf) do
        #{:server, :RsQuestObjectiveComplete, <<0, 0, 0, 0>>}
        <<0,0,0,0>> = buf
        %{error: 0}
    end
    def parse(:RsQuestTeleport, buf) do
        <<error::32-little>> = buf
        %{error: error}
    end

    def parse(:RsQuickSlotSet, buf) do
        #{:server, :RsQuickSlotSet, <<0, 0, 0, 0, 21, 0, 1, 17, 223, 12, 0, 0, 21, 4, 0, 43, 130, 103, 30, 0, 0, 0, 0, 0>>}
        %{bin: buf}
    end

    def parse(:NtExpUpdate, buf) do
        #{:server, :NtExpUpdate, <<0, 145, 1, 2>>}
        {exp, rest} = Proto.varint_read buf
        {gained_exp, _} = Proto.varint_read rest
        %{exp: exp, gained_exp: gained_exp}
    end

    def parse(:NtLevelUpdate, buf) do
        <<current_level, rest::binary>> = buf
        #{:server, :NtLevelUpdate, <<2, 2, 0>>}
        %{bin: buf, level: current_level}
    end

    def parse(:RsNpcShopBuyGoods, buf) do
        <<error::32-little, npc_id::64-little, rest::binary>> = buf
        {count, rest} = Proto.varint_read rest
        {_, goods} = Enum.reduce 1..count, {rest, []}, fn(_, {rest, acc})->
            <<type::32-little, amount::32-little, rest::binary>> = rest
            good = {type, amount}
            {rest, acc ++ [good]}
        end

        %{error: error, npc_id: npc_id, goods: goods}
    end


    def parse(:RsSelectServer, buf) do
        #{:server, :RsSelectServer, <<0, 0, 0, 0, 83, 0>>}
        <<error::32-little, server_id::little-16>> = buf
        error_type = case error do
            0 -> :ok
            8970 -> :queued
            _ -> error
        end
        %{server_id: server_id, error: error, error_type: error_type}
    end
    def parse(:RsGetWaitPos, buf) do
        <<error::32-little, server_id::16-little, pos::16-little, _::binary>> = buf
        %{error: error, server_id: server_id, pos: pos}
    end

    def parse(:RsMoveToServer, buf) do
        #{:server, :RsMoveToServer, <<0, 0, 0, 0, 83, 0, 12, 49, 56, 51, 46, 49, 49, 49, 46, 49, 46, 56, 51, 16, 52>>}
        <<0,0,0,0,server_id::little-16, _::binary>> = buf
        %{server_id: server_id}
    end

    def parse(:RsListMyCharacters, buf) do
        #{:server, :RsListMyCharacters, <<0, 0, 0, 0, 2, 83, 0, 7, 97, 114, 99, 104, 101, 114, 49, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 186, 42, 2, 0, 203, 113, 255, 15, 111, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 85, 0, 3, 107, 106, 117, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 186, 42, 2, 0, 63, 131, 172, 9, 111, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1>>}
        case buf do
            <<0,0,0,0,0,1,1>> ->
                %{chars: []}
            #TODO: consider >1 char
            <<0,0,0,0,1,server_id::little-16, n_len, name::binary-size(n_len), _::binary>> ->
                %{chars: [%{server_id: server_id, name: name}]}
            _ ->
                %{chars: :more_than_one}
        end
    end

    def parse(:RsLogin, buf) do
        case buf do
            <<10, 34, 0, 0, _::binary>> ->
                %{error: 8714}
            <<0,0,0,0,
            0,0,0,0, #error?
            1, #only support 1 char for now
            char_id::32-little,
            cn_s, char_name::binary-size(cn_s),
            _::binary
            >> ->
                %{char_id: char_id, char_name: char_name, error: 0}
            <<0,0,0,0,0,0,0,0,_::binary>> ->
                %{error: 0}
        end
    end

    def parse(:RsLoadComplete, buf) do
        <<error::32-little>> = buf
        %{error: error}
    end

    def parse(:RsListServer, buf) do

        <<error::32-little, rest::binary>> = buf
        {count, rest} = Proto.varint_read(rest)

        {rest, server_list} =
          Enum.reduce 1..count, {rest, []}, fn( _iteration, {rest, acc}) -> parse_server_lists(rest, acc) end

        <<current_page, total_page>> = rest

        %{error: error, server_list: server_list, current_page: current_page, total_page: total_page}
    end

    def parse(:RsTradePurchaseTake, bin) do
        <<result::32-little, len, sale_id::binary-size(len), rest::binary>> = bin
        %{result: result, sale_id: sale_id, rest: rest}
    end

    def parse(:NtPcDieInfo, buf) do

        #void FUN_7ff7879dd2e0(longlong param_1)

        # *(undefined *)(param_1 + 0x10) = readbyte() FUN_7ff7879bcc60(param_2,"_now",param_1 + 0x10);
        # read_qword(param_1 + 0x18); FUN_7ff78797f5e0(param_2,"_reg_time",param_1 + 0x18);
        # read_bytearray((void *)(param_1 + 0x20)); FUN_7ff78797f1f0(param_2,"_pc_killer_nickname",param_1 + 0x20);
        # read_bytearray((void *)(param_1 + 0x30)); FUN_7ff78797f1f0(param_2,"_pc_killer_guildname",param_1 + 0x30);
        # read_short((void *)(param_1 + 0x40)); FUN_7ff7879b3e30(param_2,"_pc_killer_guild_emblem_id",param_1 + 0x40);
        # read_uint32((int *)(param_1 + 0x44));   FUN_7ff7879b4170(param_2,"_monster_killer_data_id",(longlong *)(param_1 + 0x44));
        # read_qword(param_1 + 0x48); FUN_7ff78797f5e0(param_2,"_guild_id",param_1 + 0x48);
        # read_qword(param_1 + 0x50); FUN_7ff78797f5e0(param_2,"_exp",param_1 + 0x50);
        # read_uint32_list(); FUN_7ff7879b4310(param_2,"_item_ids",param_1 + 0x58);
        # read_bytelist((void *)(param_1 + 0x70)); FUN_7ff7879b3ca0(param_2,"_enchant_levels",param_1 + 0x70);
        # *(undefined *)(param_1 + 0x88) = readbyte() FUN_7ff7879bcc60(param_2,"_revived",param_1 + 0x88);


        #{:decoded_packet, :NtPcDieInfo,
        # <<0, 21, 183, 95, 81, 111, 1, 0, 0, 0, 0, 0, 0, 216, 177, 1, 0, 0, 0, 0, 0, 0,
        #   0, 0, 0, 193, 166, 0, 0, 0, 0, 0, 0, 0, 0, 0>>}
        %{buf: buf}
    end

    def parse(:RsDetermineBonusStat, buf) do
        #:RsDetermineBonusStat, <<0, 0, 0, 0, 6, 1, 0, 0, 0, 0, 2, 11, 0, 0, 0, 3, 1, 0, 0, 0, 4, 0, 0, 0, 0, 5, 0, 0, 0, 0, 6, 0, 0, 0, 0>>
        <<error::32-little, count, rest::binary>> = buf
        {_,stats} = Enum.reduce 1..count, {rest, []}, fn(_, {rest, acc}) ->
            <<type, amount::32-little, rest::binary>> = rest
            {rest, acc++[{type, amount}]}
        end

        %{error: error, stats: stats}
    end

    def parse(:RsBuyBMShopDisplayGoods, rest) do
        #<<6, 41, 0, 0,
        #240, 3, 0, 0,
        #249, 240, 240, 17, 249, 240, 240, 17, 1, 0, 0, 0, 232, 253, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1>>
        <<error::32-little, shop_id::32-little, item_type::32-little, rest::binary>> = rest
        %{error: error, shop_id: shop_id, item_type: item_type}
    end

    def parse(:RsLoadDailyRewardInfo, rest) do
        %{daily_rewards: [], bm_daily_rewards: [], result: 0}
    end
    def parse(:RsLoadDailyRewardInfo, rest) do
        {count, rest} = Proto.varint_read(rest)
        itr = :lists.seq(0, count-1)
        {rest, daily_rewards} = Enum.reduce itr, {rest, []}, fn(_, {rest, acc})->
            <<id::32-little, version_no::32-little, current_step_id::16-little,
                last_updated_at::64-little, pending, rest::binary>> = rest
            reward = %{id: id, version_no: version_no, current_step_id: current_step_id,
                last_updated_at: last_updated_at, pending: pending}
            {rest, acc ++ [reward]}
        end

        {count, rest} = Proto.varint_read(rest)
        itr = :lists.seq(0, count-1)
        {rest, bm_daily_rewards} = Enum.reduce itr, {rest, []}, fn(_, {rest, acc})->
            <<id::32-little, current_step_id::16-little,
                last_updated_at::64-little, rest::binary>> = rest
            reward = %{id: id, current_step_id: current_step_id,
                last_updated_at: last_updated_at}
            {rest, acc ++ [reward]}
        end

        #<<0,0,result::32-little>> = rest
        result = 0
        %{daily_rewards: daily_rewards, bm_daily_rewards: bm_daily_rewards, result: result}
    end

    def parse(:RsTakeDailyReward, rest) do
        <<error::32-little, _::binary>> = rest
        %{error: error}
    end



    def parse(:RsAchievements, rest) do
        {count, rest} = Proto.varint_read(rest)
        itr = :lists.seq(0, count-1)
        {rest, achievements} = Enum.reduce itr, {rest, []}, fn(_, {rest, acc})->
            <<id::32-little, rest::binary>> = rest
            {count, rest} = Proto.varint_read rest
            {reward_order, rest} = Proto.varint_read rest
            needed = Type.achievement(id, reward_order+1)
            achievement = %{id: id, count: count, reward_order: reward_order, needed: needed}
            {rest, acc ++ [achievement]}
        end
        %{achievements: achievements}
    end
    def parse(:NtAchievementUpdateCount, rest) do
        <<id::32-little, rest::binary>> = rest
        {count, _} = Proto.varint_read(rest)
        %{id: id, count: count}
    end
    def parse(:RsAchievementTakeReward, rest) do
        <<error::32-little, id::32-little, reward_order::32-little>> = rest
        needed = Type.achievement(id, reward_order+1)
        %{error: error, id: id, reward_order: reward_order, needed: needed}
    end

    def parse(:RqTelpo, <<type::32-little, gate_id::64-little>>) do
        %{type: type, gate_id: gate_id}
    end

    def parse(:RsDeathPenaltyInfo, rest) do
        #<<4,
        #1, 0, 0, 0, 2, 54, 104, 228, 111, 1, 0, 0, 80, 12, 4, 0, 0, 0, 0, 0, 34, 0, 0, 0,
        #3, 0, 0, 0, 54, 221, 90, 228, 111, 1, 0, 0, 80, 12, 4, 0, 0, 0, 0, 0, 34, 0, 0, 0,
        #4, 0, 0, 0, 250, 199, 91, 228, 111, 1, 0, 0, 80, 12, 4, 0, 0, 0, 0, 0, 34, 0, 0, 0,
        #5, 0, 0, 0, 4, 217, 99, 228, 111, 1, 0, 0, 80, 12, 4, 0, 0, 0, 0, 0, 34, 0, 0, 0,
        #3,
        #1, 0, 0, 0, 118, 208, 39, 228, 111, 1, 0, 0, 252, 11, 238, 11, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        #2, 0, 0, 0, 184, 143, 48, 228, 111, 1, 0, 0, 245, 188, 140, 12, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        #3, 0, 0, 0, 66, 218, 61, 228, 111, 1, 0, 0, 227, 167, 232, 17, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        #246, 20, 114, 228, 111, 1, 0, 0, 1, 5, 0, 0>>
        {exp_count, rest} = Proto.varint_read(rest)
        itr = :lists.seq(0, exp_count-1)
        {rest, death_penalty_xp} = Enum.reduce itr, {rest, []}, fn(_, {rest, acc})->
            <<id::32-little, reg_time::64-little, exp::64-little, char_level::32-little, rest::binary>> = rest
            entry = ~M{id, reg_time, exp, char_level}
            {rest, acc ++ [entry]}
        end

        {item_count, rest} = Proto.varint_read(rest)
        itr = :lists.seq(0, item_count-1)
        {rest, death_penalty_item} = Enum.reduce itr, {rest, []}, fn(_, {rest, acc})->
            <<id::32-little, reg_time::64-little, item_id::32-little, is_locked, enchant_lvl,
            is_break, _::binary-15, rest::binary>> = rest
            entry = ~M{id, reg_time, item_id, is_locked, enchant_lvl, is_break, }
            {rest, acc ++ [entry]}
        end

#FUN_7ff67f60f1e0(acStack200,0x80,"%s:vec[size=%d]","_death_penalty_exps");
  #FUN_7ff67f60f1e0(acStack200,0x80,"%s:vec[size=%d]","_death_penalty_items");
  #FUN_7ff67ff70bf0(param_2,"_free_restore_last_reset_time",param_1 + 0x40);
  #FUN_7ff67ffa5d00(param_2,"_free_restore_use_count",param_1 + 0x48);
  #FUN_7ff67ffa5d00(param_2,"_free_restore_max_count",param_1 + 0x49);
  #FUN_7ff67ffa6030(param_2,"_exp_restore_charge_point",param_1 + 0x4a);

        %{death_penalty_xp: death_penalty_xp, death_penalty_item: death_penalty_item}
    end
    def parse(:RsRestoreDeathPenaltyExp, rest) do
        #<<2, 0, 0, 0, 0, 0, 0, 0, 1, 5>>
        <<index::32-little, error::32-little, _::binary>> = rest
        %{index: index, error: error}
    end
    def parse(:RsRestoreDeathPenaltyItem, rest) do
        #<<4, 0, 0, 0, 0, 0, 0, 0>>
        <<index::32-little, error::32-little>> = rest
        %{index: index, error: error}
    end

    def parse(:NtWorldMapLuckyMonsterStart, rest) do
        #<<12, 0, 0, 0, 128, 230, 166, 71, 0, 118, 59, 71, 130, 233, 159, 69>>
        <<index::32-little, x::32-little-float, y::32-little-float, z::32-little-float>> = rest
        %{index: index, pos: {trunc(x), trunc(y), trunc(z)}}
    end
    def parse(:NtWorldMapLuckyMonsterEnd, rest) do
        #<<1, 0, 0, 0>>
        <<index::32-little>> = rest
        %{index: index}
    end

    def parse(:NtSleepMode, rest) do
        #<<1, 0, 0, 0>>
        <<flag>> = rest
        %{flag: flag}
    end

    def parse(:NtHitByPcInSleepMode, rest) do
        #<<145, 13>>
        {char_id, ""} = Proto.varint_read(rest)
        %{char_id: char_id}
    end

    def parse(:NtKick, rest) do
        #<<1, 0, 0, 0>>
        <<reason::16-little>> = rest
        %{reason: reason}
    end

    #def parse(:NtSkillEffect, rest) do
    #    %{}
    #end

    def parse(_, bin) do
        %{bin: bin}
    end

    def parse_server_lists(bitstring, acc) do
        <<  server_id::little-16,
            len, server_name::binary-size(len),
            is_running,
            tags,
            play_count_max::little-16,
            play_count_now::little-16,
            wait_count_now::little-16,
            in_maintenance,
            congestion_ratio,
            rest::binary        >> = bitstring

        updated_acc = acc++[%{
            server_id: server_id,
            server_name: server_name,
            is_running: is_running,
            tags: tags,
            play_count_max: play_count_max,
            play_count_now: play_count_now,
            wait_count_now: wait_count_now,
            in_maintenance: in_maintenance,
            congestion_ratio: congestion_ratio
        }]
        {rest, updated_acc}
    end

    def encode(o=:RqCharCreate, name) do  #2 - 12 chars
        <<Opcodes.opcode(o)::little-16,
            byte_size(name), name::binary,
            2, 0, 0, 0, 186, 42, 2, 0>>  #elf with bow?
    end

    def encode(o=:RqSelectCharacter, char_id) do
        <<Opcodes.opcode(o)::little-16,
            char_id::little-32,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0>>
    end

    def encode(o=:HeartBeat, time_bin) do
        <<Opcodes.opcode(o)::little-16,
            time_bin::binary>>
    end

    def encode(:RqCharMoveStart, char_id, s, d), do:
        encode(:RqCharMoveStart, %{char_id: char_id, source: s, delta: d})
    def encode(:RqCharMoveStart, %{char_id: char_id, source: {x,y,z}, delta: {x1,y1,z1}}) do
        <<Opcodes.opcode(:RqCharMoveStart)::little-16,
            LM2.Model.Packet.encode_varint(char_id) :: binary,
            x::float-32-little,y::float-32-little,z::float-32-little,
            x1::float-32-little,y1::float-32-little,z1::float-32-little>>
    end

    def encode(:RqCharMove, char_id, s, d), do:
        encode(:RqCharMove, %{char_id: char_id, source: s, delta: d})
    def encode(:RqCharMove, %{char_id: char_id, source: {x,y,z}, delta: {x1,y1,z1}}) do
        <<Opcodes.opcode(:RqCharMove)::little-16,
            LM2.Model.Packet.encode_varint(char_id) :: binary,
            x::float-32-little,y::float-32-little,z::float-32-little,
            x1::float-32-little,y1::float-32-little,z1::float-32-little>>
    end

    def encode(:RqCharMoveStop, char_id, s), do:
        encode(:RqCharMoveStop, %{char_id: char_id, source: s})
    def encode(:RqCharMoveStop, %{char_id: char_id, source: {x,y,z}}) do
        <<Opcodes.opcode(:RqCharMoveStop)::little-16,
            LM2.Model.Packet.encode_varint(char_id) :: binary,
            x::float-32-little,y::float-32-little,z::float-32-little>>
    end

    def encode(o=:RqItemUse, item_id) do
        #{:client, :RqItemUse, <<223, 130, 13, 0, 0, 92, 2, 0>>}
        <<Opcodes.opcode(o)::little-16,
            item_id::little-64>>
    end
    def encode(o=:RqItemUseSelectBox, item_id, index) do
        #{:client, :RqItemUse, <<223, 130, 13, 0, 0, 92, 2, 0>>}
        <<Opcodes.opcode(o)::little-16,
            item_id::little-64, index>>
    end

    def encode(o=:RqNpcTouch, npc_Id) do
        #{:client, :RqNpcTouch, <<43, 33, 3, 0, 0, 0, 0, 0>>}
        <<Opcodes.opcode(o)::little-16,
            npc_Id::little-64>>
    end

    def encode(o=:RqQuestAccept, quest_id) do
        #{:client, :RqQuestAccept, <<2, 0, 0, 0>>}
        <<Opcodes.opcode(o)::little-16,
            quest_id::little-32>>
    end
    def encode(o=:RqGuideQuestAccept, quest_id) do
        #{:client, :RqQuestAccept, <<2, 0, 0, 0>>}
        <<Opcodes.opcode(o)::little-16,
            quest_id::little-32>>
    end
    def encode(o=:RqQuestTakeReward, quest_id, extra) do
        #{:client, :RqQuestTakeReward, <<1, 0, 0, 0, 0, 0, 0, 0>>}
        <<Opcodes.opcode(o)::little-16,
            quest_id::little-32, extra::32-little>>
    end
    def encode(o=:RqQuestObjectiveComplete, obj) do
        #{:client, :RqQuestObjectiveComplete, "/"}  47
        #{:client, :RqQuestObjectiveComplete, "0"}  48
        <<Opcodes.opcode(o)::little-16,
            obj::binary>>
    end
    def encode(o=:RqQuestTeleport, quest_id) do
        #{:client, :RqQuestTeleport, <<5, 0, 0, 0>>
        <<Opcodes.opcode(o)::little-16,
            quest_id::little-32>>
    end

    def encode(o=:RqQuickSlotSet, quest_id) do
        #haste pot on quick slot
        #{:client, :RqQuickSlotSet, <<21, 0, 1, 17, 223, 12, 0, 0, 21, 4, 0, 43, 130, 103, 30, 0, 0, 0, 0, 0>>}
        #set it to auto?
        #{:client, :RqQuickSlotSet, <<21, 0, 1, 17, 223, 12, 0, 0, 21, 4, 0, 43, 130, 103, 30, 0, 0, 0, 0, 1>>}
        <<Opcodes.opcode(o)::little-16,
            quest_id::little-32, 0::32>>
    end

    def encode(o=:NtAutoHuntToggle, v) do
        #{:client, :NtAutoHuntToggle, <<1>>}
        <<Opcodes.opcode(o)::little-16,
           v>>
    end

    def encode(o=:RqCharRespawn, char_id) do
        <<Opcodes.opcode(o)::little-16,
           LM2.Model.Packet.encode_varint(char_id)::binary>>
    end

    def encode(o=:RqItemEnchant, scroll_uid, equip_uid) do
        <<Opcodes.opcode(o)::little-16,
           scroll_uid::64-little,
           equip_uid::64-little>>
    end

    def encode(o=:RqItemSmelt, scroll_uid, equip_uid, is_curse) do
        <<Opcodes.opcode(o)::little-16,
           scroll_uid::64-little,
           equip_uid::64-little,
           is_curse>>
    end

    def encode(o=:RqBasicAtkToggleOn, char_id, mob_id) do
        #{:client, :RqBasicAtkToggleOn, <<211, 126, 201, 188, 0, 0, 0, 0, 0, 0>>}
        <<Opcodes.opcode(o)::little-16,
           LM2.Model.Packet.encode_varint(char_id)::binary, mob_id::64-little>>
    end
    def encode(o=:RqBasicAtkToggleOff, char_id) do
        #{:client, :RqBasicAtkToggleOff, <<211, 126>>}
        <<Opcodes.opcode(o)::little-16,
           LM2.Model.Packet.encode_varint(char_id)::binary>>
    end

    def encode(o=:RqSkillCast, char_id, skill_id, mob_id) do
        <<Opcodes.opcode(o)::little-16,
           LM2.Model.Packet.encode_varint(char_id)::binary,
           skill_id::32-little,
           mob_id::64-little>>
    end
    def encode(o=:RqSkillCast, char_id, skill_id, target_key, parts_id) do
        <<Opcodes.opcode(o)::little-16,
           LM2.Model.Packet.encode_varint(char_id)::binary,
           skill_id::32-little,
           target_key::32-little,
           parts_id::32-little>>
    end


    def encode(o=:RqSwitchToggle, bin_switch) do
        #{:client, :RqSwitchToggle, <<1, 1>>}
        <<Opcodes.opcode(o)::little-16,
           bin_switch::binary>>
    end


    def encode(o=:RqFOInteraction, id) do
        #{:client, :RqFOInteraction, <<165, 241, 0, 0>>
        <<Opcodes.opcode(o)::little-16,
           id::32-little>>
    end

    def encode(o=:RqLoadPost, bin) do
        <<Opcodes.opcode(o)::little-16,
           bin::binary>>
    end


    def encode(o=:KeepAlive) do
        <<Opcodes.lounge_opcode(o)::little-16>>
    end
    def encode(o=:RqSelectServer, server_id) do
        #{:client, :RqSelectServer, <<83, 0>>}
        <<Opcodes.lounge_opcode(o)::little-16,
           server_id::little-16>>
    end
    def encode(o=:RqGetWaitPos, server_id) do
        #{:client, :RqGetWaitPos, <<83, 0>>}
        <<Opcodes.lounge_opcode(o)::little-16,
           server_id::little-16>>
    end
    def encode(o=:RqMoveToServer, server_id) do
        #{:client, :RqMoveToServer, <<83, 0>>}
        <<Opcodes.lounge_opcode(o)::little-16,
           server_id::little-16>>
    end

    def encode(o=:RqListServer) do
        <<Opcodes.lounge_opcode(o)::little-16>>
    end
    def encode(o=:RqListMyCharacters) do
        <<Opcodes.lounge_opcode(o)::little-16>>
    end



    def encode(o=:RqLoadComplete, bin) do
        <<Opcodes.opcode(o)::little-16,
           bin::binary>>
    end

    def encode(o=:RqRetrieveBMShopDisplayGoods, bin) do
        <<Opcodes.opcode(o)::little-16,
           bin::binary>>
    end


    def encode(o=:RqBuyBMShopDisplayGoods, shop_id, goods_id, amount) do
        #dai
        #{1577754250013, :client, :RqBuyBMShopDisplayGoods, <<240, 3, 0, 0, 249, 240, 240, 17, 1, 0, 0, 0>>, %{bin: <<240, 3, 0, 0, 249, 240, 240, 17, 1, 0, 0, 0>>}}
        <<Opcodes.opcode(o)::little-16,
           shop_id::32-little, goods_id::32-little, amount::32-little>>
    end
    def encode(o=:RqAgathionSummon, id) do
        <<Opcodes.opcode(o)::little-16,
           id::32-little>>
    end
    def encode(o=:RqAgathionCombine, list) do
        #{:client, :RqAgathionCombine, <<3, 102, 0, 0, 0, 2, 0, 0, 0, 103, 0, 0, 0, 1, 0, 0, 0, 106, 0, 0, 0, 1, 0, 0, 0>>
        bin = Enum.reduce(list, "", fn({type, amount}, acc)->
            acc <> <<type::32-little, amount::32-little>>
        end)
        <<Opcodes.opcode(o)::little-16,
           length(list), bin::binary>>
    end

    def encode(o=:RqClassCardTransform, id) do
        <<Opcodes.opcode(o)::little-16,
           id::32-little>>
    end
    def encode(o=:RqClassCardCombine, list) do
        bin = Enum.reduce(list, "", fn({type, amount}, acc)->
            acc <> <<type::32-little, amount::32-little>>
        end)
        <<Opcodes.opcode(o)::little-16,
           length(list), bin::binary>>
    end

    def encode(o=:RqTelpo, bin) do
        <<Opcodes.opcode(o)::little-16,
           bin::binary>>
    end
    def encode(o=:RqTelpo, type, gate_id) do
        <<Opcodes.opcode(o)::little-16,
           type::32-little, gate_id::64-little>>
    end
    def encode(o=:RqNpcShopBuyGoods, npc_id, goods) do
        #{:client, :RqNpcShopBuyGoods, <<48, 218, 0, 0, 0, 0, 0, 0, 1, 121, 131, 132, 12, 2, 0, 0, 0>>
        bin = Enum.reduce(goods, "", fn({type, amount}, acc)->
            acc <> <<type::32-little, amount::32-little>>
        end)
        <<Opcodes.opcode(o)::little-16,
           npc_id::64-little, length(goods), bin::binary>>
    end
    def encode(o=:RqGetDropItem, id) do
        <<Opcodes.opcode(o)::little-16,
           id::64-little>>
    end
    def encode(o=:RqTradeSell, struct) do

        %{sale_item_key: sale_item_key,
          sale_item_count: sale_item_count,
          sale_price_item_count: sale_price_item_count,
          reg_fee_rate: reg_fee_rate,
          reg_fee_min: reg_fee_min} = struct

        <<Opcodes.opcode(o)::little-16, sale_item_key::64-little, sale_item_count::64-little,
            sale_price_item_count::64-little, reg_fee_rate::64-little, reg_fee_min::64-little>>
    end

    def encode(o=:RqTradeCancelSaleTake, struct) do
            %{sale_id: sale_id, sale_item_id: sale_item_id, sale_item_count: sale_item_count, expired: expired} = struct

            sale_id_size = LM2.Model.Packet.encode_varint(byte_size(sale_id))

            <<Opcodes.opcode(o)::little-16,
                sale_id_size::binary, sale_id::binary,
                sale_item_id::32-little, sale_item_count::64-little, expired>>
    end


    def encode(o=:RqRetrievePushRewardInfo, _struct) do
        <<Opcodes.opcode(o)::little-16>>
    end

    def encode(o=:RqTradePurchaseTake, struct) do

        %{sale_id: sale_id,
          sale_item_id: sale_item_id,
          sale_item_count: sale_item_count} = struct


        <<Opcodes.opcode(o)::little-16,
            LM2.Model.Packet.encode_varint(byte_size(sale_id))::binary,
            sale_id::binary,
            sale_item_id::32-little,
            sale_item_count::64-little >>
    end
    def encode(o=:RqTradeEarningsTake, delivery_ids) do

        ids = Enum.reduce delivery_ids, "", fn(id, acc)->
            acc <> LM2.Model.Packet.encode_varint(byte_size(id)) <> id
        end
        <<Opcodes.opcode(o)::little-16,
            LM2.Model.Packet.encode_varint(Enum.count(delivery_ids))::binary,
            ids::binary >>
    end

    def encode(o=:RqDetermineBonusStat, stats) do
        #{:client, :RqDetermineBonusStat, <<6,
        #1, 0, 0, 0, 0,
        #2, 11, 0, 0, 0,
        #3, 1, 0, 0, 0,
        #4, 0, 0, 0, 0,
        #5, 0, 0, 0, 0,
        #6, 0, 0, 0, 0>>
        bin = Enum.reduce(stats, "", fn({type, amount}, acc)->
            acc <> <<type, amount::32-little>>
        end)
        <<Opcodes.opcode(o)::little-16,
          length(stats), bin::binary>>
    end

    def encode(o=:RqTakePushReward, id) do
        <<Opcodes.opcode(o)::little-16, id::48-little>>
    end

    def encode(o=:RqAchievementTakeReward, id, order) do
        <<Opcodes.opcode(o)::little-16, id::32-little, order::32-little>>
    end

    def encode(o=:RqCraftItem, craft_id, amount, bin) do
        <<Opcodes.opcode(o)::little-16, craft_id::32-little, amount::32-little, bin::binary>>
    end

    def encode(o=:RqItemRemove, item_id, amount) do
        <<Opcodes.opcode(o)::little-16, item_id::64-little, amount::64-little>>
    end

    def encode(o=:RqTakeDailyReward, type, amount) do
        <<Opcodes.opcode(o)::little-16, type::32-little, amount::32-little>>
    end

    def encode(o=:RqItemDecompose, items) do
        bin = Enum.reduce(items, "", fn({id, amount}, acc)->
            acc <> <<id::64-little, amount::32-little>>
        end)
        <<Opcodes.opcode(o)::little-16,
           length(items), bin::binary>>
    end

    def encode(o=:RqRestoreDeathPenaltyExp, index) do
        <<Opcodes.opcode(o)::little-16, index::32-little, 1, 1>>
    end
    def encode(o=:RqRestoreDeathPenaltyItem, index) do
        <<Opcodes.opcode(o)::little-16, index::32-little, 1>>
    end

    def encode(o=:RqSleepMode, flag) do
        <<Opcodes.opcode(o)::little-16, flag>>
    end

    def encode(o=:NtCharAppear, map) do
        #%{id: key, nickname: nickname,
        #    pc_id: pc_id, dbid: dbid, pos: {x,y,z},
        #    flags: flags, state_flags: state_flags, statue_mode: statue_mode}
        {x,y,z} = map.pos
        piece = <<
            Opcodes.opcode(o)::little-16,
            LM2.Model.Packet.encode_varint(map.id)::binary,
            map.flags::16-little
        >>
        piece2 = if !!map[:nickname] do
            <<1, Proto.write_byteArray(map.nickname)::binary>>
        else <<0>> end
        piece3 = <<
            map.pc_id::32-little,
            map.state_flags,
            x::32-little-float,
            y::32-little-float,
            z::32-little-float,
            map.blob::binary,
            map.dbid::64-little,
            map.statue_mode
        >>
        piece <> piece2 <> piece3
    end

    def encode(o) do
        <<Opcodes.opcode(o)::little-16>>
    end
end
