{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE DeriveGeneric #-}
module DSL.Kaitai where

import Control.Applicative ((<|>))
import qualified Data.Char as C
import           GHC.Generics (Generic)
import           Data.List.NonEmpty (NonEmpty)
import           Data.Map           (Map)
import           Data.IntMap           (IntMap)
import           Data.Text          (Text)
import qualified Data.Text          as T
import qualified Data.Aeson as A

import qualified Data.Attoparsec.Text as P

import qualified Data.Yaml as Y
import Data.Yaml (parseJSON, FromJSON(..), (.:), (.:?))

data Type = Type {
    meta   :: Maybe Meta,
    doc    :: Maybe Text, -- Use it for generating haddoc markup
    --docRef :: Maybe Text,
    seq    :: NonEmpty Attribute,
    --instances   :: Ins,
    enums  :: Maybe (Map Text KaiEnum),
    types  :: Maybe (Map Text Type)
}   deriving (Show, Generic)

instance FromJSON Type

type KaiEnum = IntMap Text

data TypeAnnotation
    = UserDefined Text -- must match a type in scope
    | Word      Int (Maybe Endian)
    | Integer   Int (Maybe Endian)-- parser must check for correctness
    | Float     Int (Maybe Endian)
   -- | BitSizeInt Text
    -- ByteArrays are default if there's no type mentioned
    | ByteArray -- must check for size or size-eos: true
        deriving (Show, Generic)

pNum = P.parseOnly go
  where
    go = u <|> s <|> f <|> (UserDefined <$> pIdentifier)

    -- parse how size in bytes, but return in bits
    num :: Char -> P.Parser Int
    num = fmap ((*8) . read . pure) . P.char
    s18 = num '1' <|> num '2' <|> s48
    s48 = num '4' <|> num '8'
    end =   ("le" *> pure (Just LE))
        <|> ("be" *> pure (Just BE))
        <|> pure Nothing
    u = "u" *> (Word    <$> s18 <*> end)
    s = "s" *> (Integer <$> s18 <*> end)
    f = "f" *> (Float   <$> s48 <*> end)

    pIdentifier =
        (<>) <$> fmap T.singleton P.letter <*> P.takeWhile C.isAlphaNum

instance FromJSON TypeAnnotation where
    parseJSON (Y.String s) = case pNum s of
        Left e -> (fail "failed to parse type annotation")
        Right res -> pure res


data Repeat -- repeat parsing the attribute
    = Expr Expression -- number of times specified
    | Eos  -- end of stream
    | Until Expression -- Until given predicate is true
        deriving (Show, Generic)

instance FromJSON Repeat

data Size = ExpreSize [Expression] | EosSize
        deriving (Show, Generic)

instance FromJSON Size

data Attribute = Attribute
    {   id         :: Text -- indentifier rule must apply
    ,   doc        :: Maybe Text
    ,   docRef     :: Maybe Text
        -- UTF8 strings or bytes
    ,   contents   :: Maybe Text -- (Either Text [ Either Text Int ])
    ,   typeAnn    :: Maybe TypeAnnotation
    ,   repeat     :: Maybe Repeat
        -- byte array keys
        -- also uses terminator key
    ,   size       :: Maybe Int -- Size
    ,   process    :: Maybe Processor
        -- integer keys
    ,   enum       :: Maybe Text -- name of an existing enum
        -- string keys
        -- also uses size
    ,   encoding   :: Maybe Text
        -- strz
    ,   terminator :: Maybe Char -- default is 0
        -- specifies if terminator byte should be consumed
    ,   consume    :: Maybe Bool -- default is true
        -- include terminator in the string
    ,   include    :: Maybe Bool -- default is false
        -- ignor lack of terminator or not
    ,   eosError   :: Maybe Bool -- defalut is true
    }   deriving (Show, Generic)

instance FromJSON Attribute where
    parseJSON (Y.Object v) = Attribute
        <$> v.: "id"
        <*> v.:? "doc"
        <*> v.:? "doc-ref"
        <*> v.:? "contents"
        <*> v.:? "type"
        <*> v.:? "repeat"
        <*> v.:? "size"
        <*> v.:? "process"
        <*> v.:? "enum"
        <*> v.:? "encoding"
        <*> v.:? "terminator"
        <*> v.:? "consume"
        <*> v.:? "include"
        <*> v.:? "eos-rror"


data Processor
    = Xor Text -- better key type should there be young padavan
    | Rol Text
    | Ror Text
    | Zlib
        deriving (Show, Generic)

instance FromJSON Processor

data Endian = LE | BE deriving (Show)

instance FromJSON Endian where
    parseJSON (Y.String "le") = pure LE
    parseJSON (Y.String "be") = pure BE

data Meta = Meta {
    id          :: Text, -- identifier rule applies
    title       :: Maybe Text,
    application :: Maybe Text,
    imports     :: Maybe [FilePath],
    encoding    :: Maybe Text, -- name from IANA character sets registry
    endian      :: Maybe Endian
    -- TODO
    -- ks-version
    -- ks-debug
    -- ks-opaque-types
    -- licence
    -- file-extension
}   deriving (Show, Generic)

instance FromJSON Meta

-- neends improvement & undrestanding
data Expression
    = Literal Text
    | Reference Text
    | Boolean Bool
        deriving (Show, Generic)

instance FromJSON Expression

test :: IO (Either Y.ParseException Type)
test = Y.decodeFileEither "test-data/gif-header.ksy"
